const ROUTES = {
  HOME: "/",
  CURRENCY_DETAIL: "/detail/:currencyId",
  CURRENCY_MARKET: "/market/:currencyId",
  CURRENCY_RANKING: "/ranking",
  ERROR_PAGE: "*",
};

export default ROUTES;
