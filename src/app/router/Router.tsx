import { Navigate, Route, Routes } from "react-router";
import React from "react";
// Relative Components
import CurrencyDetail from "../main/currency/pages/detail/CurrencyDetail";
import CurrencyMarket from "../main/currency/pages/market/CurrencyMarket";
import CurrencyRanking from "../main/currency/pages/ranking/CurrencyRanking";
import Page404 from "../pages/Page404";
import ROUTES from "./Routes";

function Router() {
  return (
    <Routes>
      <Route
        path={ROUTES.HOME}
        element={<Navigate replace to={ROUTES.CURRENCY_RANKING} />}
      />
      <Route path={ROUTES.CURRENCY_RANKING} element={<CurrencyRanking />} />
      <Route path={ROUTES.CURRENCY_DETAIL} element={<CurrencyDetail />} />
      <Route path={ROUTES.CURRENCY_MARKET} element={<CurrencyMarket />} />
      <Route path={ROUTES.ERROR_PAGE} element={<Page404 />} />
    </Routes>
  );
}

export default Router;
