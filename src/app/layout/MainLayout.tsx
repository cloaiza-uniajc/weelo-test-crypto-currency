import { Divider } from "@mui/material";
import { makeStyles } from "@mui/styles";
import GlobalData from "app/main/global/components/GlobalData";
import React from "react";
import ScrollButton from "@core/components/scroll-button/ScrollButton";
// Relative Components
import { MainLayoutProps } from "./Layout";
import Footer from "./partials/Footer";
import Header from "./partials/Header";

const idDivContainer = "main";

const useStyles = makeStyles({
  footerContainer: {
    minHeight: "calc(20% - 1px)",
  },
  headerContainer: {
    minHeight: "10%",
  },
  mainContainer: {
    minHeight: "70%",
  },
});

/**
 * @function MainLayout
 * @brief Layout principal con estructura a tres filas: Header, Content, Footer
 * @param {boolean} isDarkTheme Flag que determina si está el tema oscuro
 * @param {function} toogleTheme Función para hacer switch entre los temas
 */
function MainLayout(props: MainLayoutProps) {
  const classes = useStyles();

  const { children, isDarkTheme, toogleTheme } = props;

  return (
    <div id={idDivContainer} className="h-screen overflow-auto">
      <ScrollButton idDivContainer={idDivContainer} />

      <Header
        className={classes.headerContainer}
        isDarkTheme={isDarkTheme}
        toogleTheme={toogleTheme}
      />

      <main className={classes.mainContainer}>
        <GlobalData />

        <section className="p-5">{children}</section>
      </main>

      <Divider />

      <Footer className={classes.footerContainer} />
    </div>
  );
}

export default MainLayout;
