import { Icon, Switch } from "@mui/material";
import clsx from "clsx";
import React from "react";
// Relative Components
import { SwitchThemeProps } from "../Layout";

/**
 * @function SwitchTheme
 * @brief Componente para alternar entre el tema claro y oscuro
 */
function SwitchTheme(props: SwitchThemeProps) {
  const { isDarkTheme, toogleTheme, className } = props;

  return (
    <div className={clsx("flex items-center justify-center", className)}>
      <Icon color={isDarkTheme ? "inherit" : "warning"}>light_mode</Icon>

      <Switch color="default" checked={isDarkTheme} onChange={toogleTheme} />

      <Icon color={isDarkTheme ? "info" : "inherit"}>dark_mode</Icon>
    </div>
  );
}

export default SwitchTheme;
