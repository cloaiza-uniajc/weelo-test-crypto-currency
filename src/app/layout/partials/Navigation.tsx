import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Button from "@core/components/button/Button";
import React from "react";
// Relative Components
import { NavigationProps } from "../Layout";
import ROUTES from "../../router/Routes";

function Navigation(props: NavigationProps) {
  const { className } = props;

  const { t } = useTranslation();

  return (
    <nav className={className}>
      <ul>
        <li>
          <Button
            title={t("header.rankingButton")}
            color="secondary"
            variant="contained"
            to={ROUTES.CURRENCY_RANKING}
            component={Link}
          >
            {t("header.rankingButton")}
          </Button>
        </li>
      </ul>
    </nav>
  );
}

export default Navigation;
