import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React from "react";
// Relative Components
import { FooterProps } from "../Layout";

function Footer(props: FooterProps) {
  const { className } = props;

  const { t } = useTranslation();

  return (
    <footer
      className={clsx(
        "flex flex-col justify-center items-center p-5",
        className
      )}
    >
      <span className="text-center mb-1">{t("footer.text")}</span>

      <span className="text-center text-sm">{t("footer.copy")}</span>
    </footer>
  );
}

export default Footer;
