import { AppBar } from "@mui/material";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React from "react";
// Relative Components
import { HeaderProps } from "../Layout";
import Navigation from "./Navigation";
import ROUTES from "../../router/Routes";
import SwitchTheme from "./SwitchTheme";
import SwitchLanguage from "./SwitchLanguage";

function Header(props: HeaderProps) {
  const { isDarkTheme, toogleTheme, className } = props;

  const { t } = useTranslation();

  return (
    <AppBar position="static" className={clsx("justify-center", className)}>
      <div
        className={
          "flex flex-col sm:flex-row justify-between items-center p-2 h-full"
        }
      >
        <Link
          to={ROUTES.HOME}
          className="hover:bg-white hover:bg-opacity-10 rounded-md p-3"
        >
          <div className="flex items-center">
            <div className="w-9 mr-3">
              <img src="/assets/logo.png" alt="Logo App" />
            </div>

            <h4 className="font-bold sm:text-4xl">{t("appName")}</h4>
          </div>
        </Link>

        <div className="flex flex-col sm:flex-row items-center justify-center">
          <SwitchTheme
            isDarkTheme={isDarkTheme}
            toogleTheme={toogleTheme}
            className="my-3 sm:my-0 sm:mx-3"
          />

          <SwitchLanguage className="my-3 sm:my-0 sm:mx-3" />

          <Navigation className="my-3 sm:my-0 sm:mx-3" />
        </div>
      </div>
    </AppBar>
  );
}

export default Header;
