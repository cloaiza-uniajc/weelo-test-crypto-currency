import { useTranslation } from "react-i18next";
import MenuPop from "@core/components/menu-pop/MenuPop";
import React from "react";
import translations from "@core/i18n/translation.config";
// Relative Components
import { SwitchLanguageProps } from "../Layout";

/**
 * @function SwitchLanguage
 * @brief Componente para cambiar el idioma
 */
function SwitchLanguage(props: SwitchLanguageProps) {
  const { t, i18n } = useTranslation();

  const { className } = props;

  function onChangeLanguage(lang: string) {
    i18n.changeLanguage(lang);
  }

  return (
    <div className={className}>
      <MenuPop
        icon="translate"
        title={t("common.changeLanguaje")}
        items={[
          {
            label: translations.es.label,
            onClick: () => onChangeLanguage(translations.es.lang),
          },
          {
            label: translations.en.label,
            onClick: () => onChangeLanguage(translations.en.lang),
          },
        ]}
      />
    </div>
  );
}

export default SwitchLanguage;
