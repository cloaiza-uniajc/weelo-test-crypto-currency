interface ThemeProps {
  toogleTheme(): void;
  isDarkTheme: boolean;
}
export interface FooterProps {
  className?: string;
}

export interface HeaderProps extends ThemeProps {
  className?: string;
}

export interface MainLayoutProps extends ThemeProps {
  children?: any;
}

export interface NavigationProps {
  className?: string;
}

export interface SwitchLanguageProps {
  className?: string;
}

export interface SwitchThemeProps extends ThemeProps {
  className?: string;
}
