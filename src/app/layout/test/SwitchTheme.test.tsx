import { act } from "react-dom/test-utils";
import { fireEvent, render, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import React from "react";
import SwitchTheme from "../partials/SwitchTheme";

describe("SwitchTheme", () => {
  const mockHandler = jest.fn();

  beforeAll(() => {
    i18n.init();
  });

  test("Toggle theme", () => {
    act(() => {
      render(<SwitchTheme isDarkTheme toogleTheme={mockHandler} />);
    });

    const inputSwitch = screen.getByRole("checkbox");

    fireEvent.click(inputSwitch);

    act(() => {
      render(<SwitchTheme isDarkTheme={false} toogleTheme={mockHandler} />);
    });

    fireEvent.click(inputSwitch);

    expect(mockHandler).toHaveBeenCalledTimes(2);
  });
});
