import { fireEvent, render, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import React from "react";
import translations from "@core/i18n/translation.config";
import SwitchLanguaje from "../partials/SwitchLanguage";

describe("SwitchLanguaje", () => {
  beforeAll(() => {
    i18n.init();

    render(<SwitchLanguaje />);
  });

  test("Change lang", () => {
    const iconButton = screen.getByText("translate");

    fireEvent.click(iconButton);

    const englishOption = screen.getByText(translations.en.label);

    fireEvent.click(englishOption);

    const spanishOption = screen.getByText(translations.es.label);

    fireEvent.click(spanishOption);
  });
});
