import { act } from "react-dom/test-utils";
import { BrowserRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import React from "react";
import MainLayout from "../MainLayout";

jest.mock("app/main/global/components/GlobalData", () => {
  return function MockGlobalData() {
    return <div>Global</div>;
  };
});

describe("MainLayout", () => {
  const childText = "Test layout";

  beforeAll(() => {
    i18n.init();
  });

  test("Render", () => {
    act(() => {
      render(
        <BrowserRouter>
          <MainLayout isDarkTheme toogleTheme={() => undefined}>
            <div>{childText}</div>
          </MainLayout>
        </BrowserRouter>
      );
    });

    const headerText = screen.getByText(i18n.t("appName").toString());

    expect(headerText).toBeInTheDocument();

    const navButton = screen.getByText(
      i18n.t("header.rankingButton").toString()
    );

    expect(navButton).toBeInTheDocument();

    const mainContent = screen.getByText(childText);

    expect(mainContent).toBeInTheDocument();

    const footerText = screen.getByText(i18n.t("footer.text").toString());

    expect(footerText).toBeInTheDocument();
  });
});
