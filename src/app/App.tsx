import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import React, { useState } from "react";
import SplashScreen from "@core/components/splash-screen/SplashScreen";
// Relative Components
import { themeOptions } from "../@core/theme/theme";
import MainLayout from "./layout/MainLayout";
import Router from "./router/Router";

function App() {
  const [theme, setTheme] = useState(themeOptions.defaultDark);
  const [isDarkTheme, setIsDarkTheme] = useState(true);

  const toogleTheme = () => {
    const isDark = !isDarkTheme;

    setIsDarkTheme(isDark);
    setTheme(isDark ? themeOptions.defaultDark : themeOptions.defaultLight);
  };

  return (
    <ThemeProvider theme={createTheme(theme)}>
      <CssBaseline />

      <SplashScreen />

      <MainLayout isDarkTheme={isDarkTheme} toogleTheme={toogleTheme}>
        <Router />
      </MainLayout>
    </ThemeProvider>
  );
}

export default App;
