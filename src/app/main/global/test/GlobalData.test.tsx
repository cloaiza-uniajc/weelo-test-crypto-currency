import { act } from "react-dom/test-utils";
import { createTheme, ThemeProvider } from "@mui/material";
import { GlobalCryptoDataResponseType } from "../Global";
import { render, screen } from "@testing-library/react";
import { themeOptions } from "@core/theme/theme";
import GlobalApiService from "../api-service/GlobalApiService";
import GlobalCryptoDataModel from "../model/GlobalCryptoDataModel";
import GlobalData from "../components/GlobalData";
import i18n from "@core/i18n/i18n";

jest.mock("../api-service/GlobalApiService");

const mockGlobalApiService: any = GlobalApiService;

describe("GlobalData", () => {
  beforeAll(() => {
    i18n.init();
  });

  test("Render", async () => {
    const response: GlobalCryptoDataResponseType = {
      coins_count: 12345,
      active_markets: 17935,
      total_mcap: 2325317030331.7476,
      total_volume: 132856294751.02643,
      btc_d: "41.31",
      eth_d: "21.43",
    };

    mockGlobalApiService.getGlobalCryptoData.mockResolvedValue(
      new GlobalCryptoDataModel({
        coins_count: response.coins_count,
        active_markets: response.active_markets,
        total_mcap: response.total_mcap,
        total_volume: response.total_volume,
        btc_d: parseFloat(response?.btc_d || "0") / 100,
        eth_d: parseFloat(response?.eth_d || "0") / 100,
      })
    );

    await act(async () => {
      render(
        <ThemeProvider theme={createTheme(themeOptions.defaultDark)}>
          <GlobalData />
        </ThemeProvider>
      );
    });

    const list = screen.getByText(i18n.t("global.coins").toString() + ":");

    expect(list).toBeInTheDocument();
  });
});
