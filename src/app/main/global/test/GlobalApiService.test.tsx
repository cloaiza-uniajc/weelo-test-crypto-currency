import Api from "@core/services/Api";
import { GlobalCryptoDataResponseType } from "../Global";
import GlobalApiService from "../api-service/GlobalApiService";
import GlobalCryptoDataModel from "../model/GlobalCryptoDataModel";

jest.mock("@core/services/Api");

const mockApi: any = Api;

describe("GlobalApiService", () => {
  describe("Get global crypto data", () => {
    test("Response ok", async () => {
      const response: GlobalCryptoDataResponseType = {
        coins_count: 6683,
        active_markets: 17935,
        total_mcap: 2325317030331.7476,
        total_volume: 132856294751.02643,
        btc_d: "40.31",
        eth_d: "21.43",
      };

      mockApi.requestAxios.mockResolvedValue([response]);

      expect(await GlobalApiService.getGlobalCryptoData()).toEqual(
        new GlobalCryptoDataModel({
          coins_count: response.coins_count,
          active_markets: response.active_markets,
          total_mcap: response.total_mcap,
          total_volume: response.total_volume,
          btc_d: parseFloat(response?.btc_d || "0") / 100,
          eth_d: parseFloat(response?.eth_d || "0") / 100,
        })
      );
    });

    test("Response empty", async () => {
      mockApi.requestAxios.mockResolvedValue([]);

      const response: any = {};

      expect(await GlobalApiService.getGlobalCryptoData()).toEqual(
        new GlobalCryptoDataModel({
          coins_count: response.coins_count,
          active_markets: response.active_markets,
          total_mcap: response.total_mcap,
          total_volume: response.total_volume,
          btc_d: parseFloat(response?.btc_d || "0") / 100,
          eth_d: parseFloat(response?.eth_d || "0") / 100,
        })
      );
    });
  });
});
