import Api from "@core/services/Api";
// Relative Components
import { GLOBAL_URL_GLOBAL_CRYPTO_DATA } from "../Global.consts";
import { GlobalCryptoDataResponseType } from "../Global";
import GlobalCryptoDataModel from "../model/GlobalCryptoDataModel";

/**
 * @class GlobalApiService
 * @brief Clase de servicio para realizar peticiones al API para el módulo Global
 */
class GlobalApiService {
  /**
   * Consulta la información global sobre las criptomonedas y mercados
   */
  async getGlobalCryptoData(): Promise<GlobalCryptoDataModel> {
    const response: GlobalCryptoDataResponseType[] = await Api.requestAxios({
      method: "GET",
      url: GLOBAL_URL_GLOBAL_CRYPTO_DATA,
    });

    const data: any = response && response?.length > 0 ? response[0] : {};

    return new GlobalCryptoDataModel({
      coins_count: data.coins_count,
      active_markets: data.active_markets,
      total_mcap: data.total_mcap,
      total_volume: data.total_volume,
      btc_d: parseFloat(data?.btc_d || "0") / 100,
      eth_d: parseFloat(data?.eth_d || "0") / 100,
    });
  }
}

export default new GlobalApiService();
