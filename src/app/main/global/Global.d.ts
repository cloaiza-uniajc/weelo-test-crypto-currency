export interface GlobalCryptoDataResponseType {
  coins_count: number;
  active_markets: number;
  total_mcap: number;
  total_volume: number;
  btc_d: string;
  eth_d: string;
}

export type GlobalCryptoDataListType = {
  icon: string;
  label: string;
  value: string;
};
