class GlobalCryptoDataModel {
  coins_count: number = 0;
  active_markets: number = 0;
  total_mcap: number = 0;
  total_volume: number = 0;
  btc_d: number = 0;
  eth_d: number = 0;

  constructor({
    coins_count = 0,
    active_markets = 0,
    total_mcap = 0,
    total_volume = 0,
    btc_d = 0,
    eth_d = 0,
  }) {
    this.coins_count = coins_count;
    this.active_markets = active_markets;
    this.total_mcap = total_mcap;
    this.total_volume = total_volume;
    this.btc_d = btc_d;
    this.eth_d = eth_d;
  }
}

export default GlobalCryptoDataModel;
