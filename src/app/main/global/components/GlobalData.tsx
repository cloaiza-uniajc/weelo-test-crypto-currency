import { CircularProgress, Icon } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { RELOAD_DATA_INTERVAL } from "@core/constants/Constants";
import { Theme } from "@mui/system";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import Utils from "@core/utils/Utils";
// Relative Components
import { GlobalCryptoDataListType } from "../Global";
import GlobalApiService from "../api-service/GlobalApiService";

const useStyles = makeStyles((theme: Theme) => ({
  label: {
    color: theme.palette.text.primary,
  },
  mainContainer: {
    backgroundColor: theme.palette.background.content,
  },
  value: {
    color: theme.palette.secondary.main,
  },
}));

/**
 * @function GlobalData
 * @brief Componente que muestra el resumen de la información global de las criptomonedas y mercados. Consulta cada cierto tiempo para actualizar la información.
 */
function GlobalData() {
  const classes = useStyles();

  const { t } = useTranslation();

  const [dataList, setDataList] = useState(
    [] as Array<GlobalCryptoDataListType>
  );
  const [skeleton, setSkeleton] = useState(true);

  useEffect(() => {
    /**
     * Consulta y procesa los datos globales obtenidos actualizando el listado a mostrar
     */
    async function getGlobalCryptoData() {
      setSkeleton(true);

      const data = await GlobalApiService.getGlobalCryptoData();

      const btc = Utils.formatNumber(data.btc_d, {
        style: "percent",
        maximumFractionDigits: 1,
      });

      const eth = Utils.formatNumber(data.eth_d, {
        style: "percent",
        maximumFractionDigits: 1,
      });

      const list = [
        {
          icon: "monetization_on",
          label: t("global.coins"),
          value: Utils.formatNumber(data.coins_count),
        },
        {
          icon: "storefront",
          label: t("global.markets"),
          value: Utils.formatNumber(data.active_markets),
        },
        {
          icon: "account_balance",
          label: t("global.capitalization"),
          value: Utils.formatNumber(data.total_mcap, { style: "currency" }),
        },
        {
          icon: "signal_cellular_alt",
          label: t("global.volume24"),
          value: Utils.formatNumber(data.total_volume, { style: "currency" }),
        },
        {
          icon: "currency_bitcoin",
          label: t("global.domain"),
          value: `BTC ${btc} - ETH ${eth}`,
        },
      ];

      setDataList(list);
      setSkeleton(false);
    }

    getGlobalCryptoData();

    const interval = setInterval(getGlobalCryptoData, RELOAD_DATA_INTERVAL);

    return () => {
      clearInterval(interval);
    };
    // eslint-disable-next-line
  }, [t]);

  return (
    <div
      className={clsx(
        "flex flex-wrap flex-col sm:flex-row sm:items-center justify-center p-4",
        classes.mainContainer
      )}
    >
      {skeleton ? (
        <CircularProgress color="secondary" size={20} />
      ) : (
        dataList.map((item, index) => (
          <div
            key={`${index}-${item.label}`}
            className="flex items-center mx-2"
          >
            <Icon fontSize="small">{item.icon}</Icon>

            <span className={clsx("font-bold text-sm mx-1", classes.label)}>
              {item.label}:
            </span>

            <span className={clsx("font-bold text-sm", classes.value)}>
              {item.value}
            </span>
          </div>
        ))
      )}
    </div>
  );
}

export default GlobalData;
