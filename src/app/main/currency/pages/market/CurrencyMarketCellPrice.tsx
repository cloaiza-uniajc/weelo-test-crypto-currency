import { Typography } from "@mui/material";
import React from "react";
import Utils from "@core/utils/Utils";
// Relative Components
import { CurrencyMarketCellPriceProps } from "../../Currency";

/**
 * @function CurrencyMarketCellPrice
 * @brief Componente para la celda de la lista de mercados que muestra el precio
 */
function CurrencyMarketCellPrice(props: CurrencyMarketCellPriceProps) {
  const { firstValue, secondValue, secondValueLabel } = props;

  return (
    <span className="flex flex-col">
      <span className="font-bold mb-2">
        {Utils.formatNumber(firstValue, {
          style: "currency",
          maximumFractionDigits: 2,
        })}
      </span>

      <Typography component="span" color="text.secondary" variant="body2">
        <span className="text-xs">
          {Utils.formatNumber(secondValue, {
            style: "currency",
            maximumFractionDigits: secondValue < 1 ? 5 : 2,
          })}
          &nbsp;
          {secondValueLabel}
        </span>
      </Typography>
    </span>
  );
}

export default CurrencyMarketCellPrice;
