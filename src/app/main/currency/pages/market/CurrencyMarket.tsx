import { Link } from "react-router-dom";
import { RELOAD_DATA_INTERVAL } from "@core/constants/Constants";
import { TableColumnsType } from "@core/components/Components";
import { Typography } from "@mui/material";
import { useParams } from "react-router";
import { useTranslation } from "react-i18next";
import Button from "@core/components/button/Button";
import ButtonUpdate from "@core/components/button-update/ButtonUpdate";
import React, { useEffect, useMemo, useState } from "react";
import ROUTES from "app/router/Routes";
import Table from "@core/components/table/Table";
import Utils from "@core/utils/Utils";
// Relative Components
import CurrencyApiService from "../../api-service/CurrencyApiService";
import CurrencyDetailError from "../detail/CurrencyDetailError";
import CurrencyMarketCellPrice from "./CurrencyMarketCellPrice";
import CurrencyMarketModel from "../../model/CurrencyMarketModel";
import CurrencyModel from "../../model/CurrencyModel";

/**
 * @function CurrencyMarket
 * @brief Listado de mercados de una moneda
 */
function CurrencyMarket() {
  const { currencyId } = useParams();

  const { t } = useTranslation();

  const [currency, setCurrency] = useState(null as unknown as CurrencyModel);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [marketsList, setMarketsList] = useState([] as CurrencyMarketModel[]);
  const [skeleton, setSkeleton] = useState(true);

  const columns = useMemo(
    () =>
      [
        {
          Header: "#",
          id: "rank",
          align: "center",
          Cell: ({ row }: any) => row.index + 1,
        },
        {
          Header: t("market.table.source"),
          accessor: "name",
          Cell: ({ row }: any) => (
            <span className="font-bold">{row.original.name}</span>
          ),
        },
        {
          Header: t("market.table.pairs"),
          id: "pairs",
          Cell: ({ row }: any) => (
            <Typography component="span" color="secondary" variant="body2">
              <span className="font-bold">
                {row.original.base}/{row.original.quote}
              </span>
            </Typography>
          ),
        },
        {
          Header: t("market.table.volume24"),
          accessor: "volume_usd",
          align: "right",
          Cell: ({ row }: any) => (
            <CurrencyMarketCellPrice
              firstValue={row.original.volume_usd}
              secondValue={row.original.volume}
              secondValueLabel={row.original.base}
            />
          ),
        },
        {
          Header: t("market.table.price"),
          accessor: "price_usd",
          align: "right",
          Cell: ({ row }: any) => (
            <CurrencyMarketCellPrice
              firstValue={row.original.price_usd}
              secondValue={row.original.price}
              secondValueLabel={row.original.quote}
            />
          ),
        },
        {
          Header: t("market.table.time"),
          accessor: "time",
          Cell: ({ row }: any) =>
            Utils.formatDate(new Date(row.original.time * 1000)),
        },
      ] as TableColumnsType,
    // eslint-disable-next-line
    [t]
  );

  async function getTickersAllCoins() {
    const data = await CurrencyApiService.getMarketsForCoin(currencyId);

    setMarketsList(data);
  }

  function onUpdateList() {
    setLoading(true);

    getTickersAllCoins().then(() => setLoading(false));
  }

  useEffect(() => {
    let interval: any;

    /**
     * Consulta la moneda a visualizar
     */
    async function getTickerCurrencyDetail() {
      setSkeleton(true);

      const { status, currency: _currency } =
        await CurrencyApiService.getTickerCurrencyDetail(currencyId);

      // + La moneda existe, se consultan los mercados
      if (status) {
        getTickersAllCoins().then(() => setSkeleton(false));

        interval = setInterval(getTickersAllCoins, RELOAD_DATA_INTERVAL);
      } else {
        setSkeleton(false);
      }

      setCurrency(_currency);
      setError(!status);
    }

    getTickerCurrencyDetail();

    return () => {
      clearInterval(interval);
    };
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <div className="flex flex-col sm:flex-row items-center justify-between mb-5">
        <h2 className="flex flex-col sm:flex-row items-center justify-center text-2xl font-bold">
          {t("market.title")}

          {!error && currency && (
            <>
              <span className="flex items-center justify-center m-2">
                {currency.name}
              </span>

              <Button
                variant="contained"
                color="secondary"
                to={Utils.getPathByParams(ROUTES.CURRENCY_DETAIL, {
                  currencyId: currency.id,
                })}
                component={Link}
              >
                {t("market.goToDetail")}
              </Button>
            </>
          )}
        </h2>

        {!error && <ButtonUpdate loading={loading} onClick={onUpdateList} />}
      </div>

      {error ? (
        <CurrencyDetailError />
      ) : (
        <Table columns={columns} data={marketsList} loading={skeleton} />
      )}
    </div>
  );
}

export default CurrencyMarket;
