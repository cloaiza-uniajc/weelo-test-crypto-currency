import { Link } from "react-router-dom";
import { Paper, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import Button from "@core/components/button/Button";
import clsx from "clsx";
import React from "react";
import ROUTES from "app/router/Routes";
import Utils from "@core/utils/Utils";
// Relative Components
import { CurrencyDetailInfoProps } from "../../Currency";
import CurrencyDetailRank from "./CurrencyDetailRank";

/**
 * @function CurrencyDetailInfo
 * @brief Componente que muestra la información principal de la moneda
 */
function CurrencyDetailInfo(props: CurrencyDetailInfoProps) {
  const { currency, className } = props;

  const { t } = useTranslation();

  return (
    <Paper
      className={clsx(
        "flex flex-col items-center justify-center p-5",
        className
      )}
      elevation={1}
    >
      <CurrencyDetailRank rank={currency.rank} className="mb-5" />

      <span className="text-xl text-center font-bold mb-5">
        {currency.name} ({currency.symbol})
      </span>

      <span className="flex items-baseline text-center mb-5">
        <span className="text-3xl font-bold mr-2">
          {Utils.formatNumber(currency.price_usd, {
            style: "currency",
            maximumFractionDigits: 2,
          })}
        </span>

        <Typography component="span" color="text.secondary" variant="body2">
          {t("currency.moneySymbol")}
        </Typography>
      </span>

      <Button
        title={t("currency.markets")}
        color="secondary"
        variant="contained"
        to={Utils.getPathByParams(ROUTES.CURRENCY_MARKET, {
          currencyId: currency.id,
        })}
        component={Link}
      >
        {t("currency.markets")}
      </Button>
    </Paper>
  );
}

export default CurrencyDetailInfo;
