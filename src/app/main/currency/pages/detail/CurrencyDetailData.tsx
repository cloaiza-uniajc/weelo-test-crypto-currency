import { Paper } from "@mui/material";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React from "react";
import Utils from "@core/utils/Utils";
// Relative Components
import { CurrencyDetailDataProps } from "../../Currency";
import CurrencyDetailDataItem from "./CurrencyDetailDataItem";

/**
 * @function CurrencyDetailData
 * @brief Componente muestra el detalle de toda la información de la moneda
 */
function CurrencyDetailData(props: CurrencyDetailDataProps) {
  const { currency, className } = props;

  const { t } = useTranslation();

  return (
    <Paper
      className={clsx(
        "flex flex-col sm:flex-row flex-wrap sm:items-center sm:justify-around p-5 w-full",
        className
      )}
      elevation={1}
    >
      <CurrencyDetailDataItem
        title={t("currency.capitalization")}
        icon="account_balance"
        value={Utils.formatNumber(currency.market_cap_usd, {
          style: "currency",
        })}
      />

      <CurrencyDetailDataItem
        title={t("currency.volume24")}
        icon="signal_cellular_alt"
        value={Utils.formatNumber(currency.volume24, {
          style: "currency",
        })}
      />

      <CurrencyDetailDataItem
        title={t("currency.range24")}
        icon="podcasts"
        value={Utils.formatNumber(currency.volume24_native, {
          style: "currency",
        })}
      />

      <CurrencyDetailDataItem
        title={t("currency.supplyC")}
        icon="local_gas_station"
        value={Utils.formatNumber(currency.csupply)}
      />

      <CurrencyDetailDataItem
        title={t("currency.supplyT")}
        icon="local_gas_station"
        value={Utils.formatNumber(currency.tsupply)}
      />

      <CurrencyDetailDataItem
        title={t("currency.supplyM")}
        icon="local_gas_station"
        value={
          Number.isNaN(currency.msupply)
            ? t("currency.noApply").toString()
            : Utils.formatNumber(currency.msupply)
        }
      />
    </Paper>
  );
}

export default CurrencyDetailData;
