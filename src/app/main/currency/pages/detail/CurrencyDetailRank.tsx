import { Container, Icon } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Theme } from "@mui/system";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React from "react";
// Relative Components
import { CurrencyDetailRankProps } from "../../Currency";

const useStyles = makeStyles((theme: Theme) => ({
  mainContainer: {
    backgroundColor: theme.palette.success.main,
    color: theme.palette.success.contrastText,
  },
}));

/**
 * @function CurrencyDetailRank
 * @brief Componente que el número en el ranking de la moneda
 */
function CurrencyDetailRank(props: CurrencyDetailRankProps) {
  const classes = useStyles();

  const { rank, className } = props;

  const { t } = useTranslation();

  return (
    <Container
      className={clsx(
        "rounded-full py-1 px-4",
        classes.mainContainer,
        className
      )}
      disableGutters
    >
      <div className="flex items-center justify-center">
        <Icon fontSize="small" className="mr-2">
          emoji_events
        </Icon>

        <span className="text-xs font-bold">{t("currency.rank")}&nbsp;</span>

        <span className="text-lg font-bold">{rank}</span>
      </div>
    </Container>
  );
}

export default CurrencyDetailRank;
