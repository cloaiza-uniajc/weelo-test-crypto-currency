import { useParams } from "react-router";
import React, { useEffect, useState } from "react";
// Relative Components
import CurrencyApiService from "../../api-service/CurrencyApiService";
import CurrencyConverter from "./CurrencyConverter";
import CurrencyDetailData from "./CurrencyDetailData";
import CurrencyDetailError from "./CurrencyDetailError";
import CurrencyDetailInfo from "./CurrencyDetailInfo";
import CurrencyModel from "../../model/CurrencyModel";
import CurrencyDetailSkeleton from "./CurrencyDetailSkeleton";

/**
 * @function CurrencyDetail
 * @brief Componente que muestra la información y detalle de una criptomoneda
 */
function CurrencyDetail() {
  const { currencyId } = useParams();

  const [currency, setCurrency] = useState(null as unknown as CurrencyModel);
  const [error, setError] = useState(false);
  const [skeleton, setSkeleton] = useState(true);

  useEffect(() => {
    /**
     * Consulta la moneda a visualizar
     */
    async function getTickerCurrencyDetail() {
      setSkeleton(true);

      const { status, currency: _currency } =
        await CurrencyApiService.getTickerCurrencyDetail(currencyId);

      setCurrency(_currency);
      setError(!status);
      setSkeleton(false);
    }

    getTickerCurrencyDetail();
    // eslint-disable-next-line
  }, []);

  if (skeleton) {
    return <CurrencyDetailSkeleton />;
  }

  return error ? (
    <CurrencyDetailError />
  ) : (
    <div>
      <div className="flex flex-col md:flex-row mb-5">
        <CurrencyDetailInfo
          currency={currency}
          className="mb-5 md:mb-0 md:mr-5"
        />

        <CurrencyDetailData currency={currency} />
      </div>

      <CurrencyConverter currency={currency} className="w-full md:w-1/2" />
    </div>
  );
}

export default CurrencyDetail;
