import { Icon, Typography } from "@mui/material";
import React from "react";
// Relative Components
import { CurrencyDetailDataItemProps } from "../../Currency";

/**
 * @function CurrencyDetailDataItem
 * @brief Componente para ítem del listado de detalles
 */
function CurrencyDetailDataItem(props: CurrencyDetailDataItemProps) {
  const { title, icon, value } = props;

  return (
    <div className="flex flex-col m-2">
      <Typography component="span" color="text.secondary" variant="body2">
        <span className="font-bold">{title}</span>
      </Typography>

      <div className="flex items-center mt-1">
        <Icon fontSize="small" className="mr-1">
          {icon}
        </Icon>

        <span className="font-bold">{value}</span>
      </div>
    </div>
  );
}

export default CurrencyDetailDataItem;
