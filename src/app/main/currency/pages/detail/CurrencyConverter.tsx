import { Icon, Input, Paper, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Theme } from "@mui/system";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React, { useState } from "react";
import Utils from "@core/utils/Utils";
// Relative Components
import { CurrencyConverterProps } from "../../Currency";

const useStyles = makeStyles((theme: Theme) => ({
  relativeIcon: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  table: {
    "& .tbl-currency-label label": {
      display: "block",
      minWidth: "100%",
      padding: "1.2rem 0.7rem 1.2rem 1.4rem",
      width: "max-content",
    },
    "& .tbl-currency-input": {
      "& input": {
        fontSize: "1.4rem",
        fontWeight: "bold !important",
        padding: "1.4rem 1.4rem 1.4rem 0.7rem",
      },
      "& > span": {
        marginRight: "1.5rem",
        marginBottom: "0.5rem",
      },
    },
  },
}));

/**
 * @function CurrencyConverter
 * @brief Componente que permite realizar la conversión entre criptomonedas y dólares
 */
function CurrencyConverter(props: CurrencyConverterProps) {
  const classes = useStyles();

  const { currency, className } = props;

  const { t } = useTranslation();

  const [cryptoCurrency, setCryptoCurrency] = useState(1);
  const [money, setMoney] = useState(Utils.roundNumber(currency.price_usd));

  function getDecimalsQty(number: number) {
    return number < 1 ? 10 : 5;
  }

  /**
   * Ejecuta la conversión de monedas actualizando los valores de ambos inputs teniendo en cuenta el input desde donde
   * fue ejecutado el cambio
   */
  const onChange = (e: any) => {
    const { value, name } = e.target;

    // + Número válido
    if (validateNumber(value) && value.toString().length < 16) {
      let _cryptoCurrency = 0;
      let _money = 0;

      const round = (number: number) =>
        /([.0])$/.test(number.toString())
          ? number
          : Utils.roundNumber(number, getDecimalsQty(number));

      switch (name) {
        case "cryptoCurrency":
          _cryptoCurrency = value;
          _money = value * currency.price_usd;
          break;
        case "money":
          _cryptoCurrency = value / currency.price_usd;
          _money = value;
          break;
      }

      setCryptoCurrency(round(_cryptoCurrency));
      setMoney(round(_money));
    }
  };

  /**
   * Valida que el texto digitado tenga un formato numérico
   * @param number Número a validar
   * @returns {boolean}
   */
  function validateNumber(number: any) {
    return number.trim() ? /^[\d]+([.]([\d]+)?)?$/.test(number) : true;
  }

  return (
    <div className={className}>
      <div className="mb-5">
        <span className="text-xl font-bold">
          {t("currency.converter", {
            cryptoCurrency: currency.symbol,
            currency: t("currency.moneySymbol"),
          })}
        </span>
      </div>

      <Paper className="flex flex-col overflow-hidden relative" elevation={1}>
        <span
          className={clsx(
            "flex items-center p-2 rounded-full justify-center top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 absolute",
            classes.relativeIcon
          )}
        >
          <Icon>sync_alt</Icon>
        </span>

        <table className={classes.table}>
          <tbody>
            <tr className="border-b">
              <td className="tbl-currency-label">
                <label htmlFor="cryptoCurrency">
                  <span className="flex items-center">
                    <Icon fontSize="large" color="warning" className="mr-2">
                      monetization_on
                    </Icon>

                    <span className="flex flex-col">
                      <Typography
                        color="text.secondary"
                        variant="body2"
                        className="mb-2"
                      >
                        {currency.symbol}
                      </Typography>

                      <span className="font-bold mt-2">{currency.name}</span>
                    </span>
                  </span>
                </label>
              </td>

              <td className="tbl-currency-input relative">
                <Input
                  id="cryptoCurrency"
                  name="cryptoCurrency"
                  type="text"
                  placeholder="0"
                  value={cryptoCurrency}
                  onChange={onChange}
                  className="w-full"
                  classes={{ input: "text-right" }}
                  disableUnderline
                />

                <Typography
                  component="span"
                  color="text.secondary"
                  className="absolute bottom-0 right-0"
                >
                  <span className="text-xs">
                    {Utils.formatNumber(cryptoCurrency, {
                      maximumFractionDigits: getDecimalsQty(cryptoCurrency),
                    })}
                  </span>
                </Typography>
              </td>
            </tr>

            <tr>
              <td className="tbl-currency-label">
                <label htmlFor="money">
                  <span className="flex items-center">
                    <Icon fontSize="large" color="success" className="mr-2">
                      monetization_on
                    </Icon>

                    <span className="flex flex-col">
                      <Typography
                        color="text.secondary"
                        variant="body2"
                        className="mb-2"
                      >
                        {t("currency.moneySymbol")}
                      </Typography>

                      <span className="font-bold mt-2">
                        {t("currency.moneyName")}
                      </span>
                    </span>
                  </span>
                </label>
              </td>

              <td className="tbl-currency-input relative">
                <Input
                  id="money"
                  name="money"
                  type="text"
                  placeholder="0"
                  value={money}
                  onChange={onChange}
                  className="w-full"
                  classes={{ input: "text-right" }}
                  disableUnderline
                />

                <Typography
                  component="span"
                  color="text.secondary"
                  className="absolute bottom-0 right-0"
                >
                  <span className="text-xs">
                    {Utils.formatNumber(money, {
                      style: "currency",
                      maximumFractionDigits: getDecimalsQty(money),
                    })}
                  </span>
                </Typography>
              </td>
            </tr>
          </tbody>
        </table>
      </Paper>
    </div>
  );
}

export default CurrencyConverter;
