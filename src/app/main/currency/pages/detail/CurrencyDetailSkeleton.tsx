import { makeStyles } from "@mui/styles";
import { Paper, Skeleton } from "@mui/material";
import React from "react";

const useStyles = makeStyles({
  skeleton: {
    "& span": {
      transform: "none",
    },
  },
});

function CurrencyDetailSkeleton() {
  const classes = useStyles();

  return (
    <div className={classes.skeleton}>
      <div className="flex flex-col md:flex-row mb-5">
        <CurrencyDetailInfoSkeleton />

        <CurrencyDetailDataSkeleton />
      </div>

      <CurrencyConverterSkeleton />
    </div>
  );
}

function CurrencyDetailInfoSkeleton() {
  return (
    <Paper
      className="flex flex-col items-center justify-center p-5 mb-5 md:mb-0 md:mr-5"
      elevation={1}
    >
      <div className="mb-5">
        <Skeleton animation="wave" width={190} height={28} />
      </div>

      <div className="mb-5">
        <Skeleton animation="wave" width={150} height={20} />
      </div>

      <div className="mb-5">
        <Skeleton animation="wave" width={200} height={30} />
      </div>

      <Skeleton animation="wave" width={105} height={40} />
    </Paper>
  );
}

function CurrencyDetailDataSkeleton() {
  return (
    <Paper
      className="flex flex-col sm:flex-row flex-wrap sm:items-center sm:justify-around p-5 w-full"
      elevation={1}
    >
      <CurrencyDetailDataItemSkeleton />
      <CurrencyDetailDataItemSkeleton />
      <CurrencyDetailDataItemSkeleton />
      <CurrencyDetailDataItemSkeleton />
      <CurrencyDetailDataItemSkeleton />
      <CurrencyDetailDataItemSkeleton />
    </Paper>
  );
}

function CurrencyDetailDataItemSkeleton() {
  return (
    <div className="m-2">
      <div className="mb-1">
        <Skeleton animation="wave" width={100} height={18} />
      </div>

      <Skeleton animation="wave" width={150} height={20} />
    </div>
  );
}

function CurrencyConverterSkeleton() {
  return (
    <div className="w-full md:w-1/2">
      <div className="mb-5">
        <Skeleton animation="wave" width={300} height={24} />
      </div>

      <Paper className="flex flex-col overflow-hidden relative" elevation={1}>
        <div className="p-6 border-b">
          <Skeleton animation="wave" width="100%" height={25} />
        </div>

        <div className="p-6">
          <Skeleton animation="wave" width="100%" height={25} />
        </div>
      </Paper>
    </div>
  );
}

export default CurrencyDetailSkeleton;
