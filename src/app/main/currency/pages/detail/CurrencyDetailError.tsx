import { Alert, AlertTitle } from "@mui/material";
import { useTranslation } from "react-i18next";
import React from "react";

/**
 * @function CurrencyDetailError
 * @brief Componente para mostrar un mensaje de error
 */
function CurrencyDetailError() {
  const { t } = useTranslation();

  return (
    <Alert severity="error">
      <AlertTitle> {t("common.errorMessage")}</AlertTitle>
      {t("currency.currencyNotFound")}
    </Alert>
  );
}

export default CurrencyDetailError;
