import { Icon, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import { RELOAD_DATA_INTERVAL } from "@core/constants/Constants";
import { TableColumnsType } from "@core/components/Components";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import ButtonUpdate from "@core/components/button-update/ButtonUpdate";
import CurrencyModel from "app/main/currency/model/CurrencyModel";
import MenuPop from "@core/components/menu-pop/MenuPop";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import ROUTES from "app/router/Routes";
import Table from "@core/components/table/Table";
import Utils from "@core/utils/Utils";
// Relative Components
import CurrencyApiService from "../../api-service/CurrencyApiService";
import CurrencyRankingCellVolume from "./CurrencyRankingCellVolume";

/**
 * @function CurrencyRanking
 * @brief Componente que muestra el listado de criptomonedas. Consulta cada cierto tiempo para actualizar la información.
 */
function CurrencyRanking() {
  const navigate = useNavigate();

  const { t } = useTranslation();

  const [coinList, setCoinList] = useState([] as CurrencyModel[]);
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(true);

  const [countTotalCoins, setCountTotalCoins] = useState(0);
  const [paginationStart, setPaginationStart] = useState(0);
  const paginationLimit = 100;

  function getCurrencyRoute(currencyId: number) {
    return Utils.getPathByParams(ROUTES.CURRENCY_DETAIL, { currencyId });
  }

  function goToDetail(currencyId: number) {
    const route = getCurrencyRoute(currencyId);

    navigate(route);
  }

  function goToMarkets(currencyId: number) {
    const route = Utils.getPathByParams(ROUTES.CURRENCY_MARKET, { currencyId });

    navigate(route);
  }

  const columns = useMemo(
    () =>
      [
        {
          Header: "#",
          accessor: "rank",
          align: "center",
        },
        {
          Header: t("ranking.table.name"),
          accessor: "name",
          Cell: ({ row }: any) => (
            <span className="flex">
              <Link
                to={getCurrencyRoute(row.original.id)}
                className="flex items-center hover:underline"
              >
                <Icon color="warning" fontSize="small" className="mr-1">
                  monetization_on
                </Icon>

                <span className="font-bold">{row.original.name} &nbsp;</span>

                <Typography
                  component="span"
                  variant="body2"
                  color="text.secondary"
                >
                  ({row.original.symbol})
                </Typography>
              </Link>
            </span>
          ),
        },
        {
          Header: t("ranking.table.price"),
          accessor: "price",
          align: "right",
          Cell: ({ row }: any) => (
            <span className="font-bold">
              {Utils.formatNumber(row.original.price_usd, {
                style: "currency",
                maximumFractionDigits: 2,
              })}
            </span>
          ),
        },
        {
          Header: t("ranking.table.hour1"),
          accessor: "percent_change_1h",
          align: "right",
          Cell: ({ row }: any) => (
            <CurrencyRankingCellVolume
              volume={row.original.percent_change_1h}
            />
          ),
        },
        {
          Header: t("ranking.table.hour24"),
          accessor: "percent_change_24h",
          align: "right",
          Cell: ({ row }: any) => (
            <CurrencyRankingCellVolume
              volume={row.original.percent_change_24h}
              background
            />
          ),
        },
        {
          Header: t("ranking.table.days7"),
          accessor: "percent_change_7d",
          align: "right",
          Cell: ({ row }: any) => (
            <CurrencyRankingCellVolume
              volume={row.original.percent_change_7d}
            />
          ),
        },
        {
          Header: t("ranking.table.market"),
          accessor: "market_cap_usd",
          align: "right",
          Cell: ({ row }: any) => (
            <span className="font-bold">
              {Utils.formatNumber(row.original.market_cap_usd, {
                style: "currency",
              })}
            </span>
          ),
        },
        {
          Header: t("ranking.table.volume24"),
          accessor: "volume24",
          align: "right",
          Cell: ({ row }: any) => (
            <span className="font-bold">
              {Utils.formatNumber(row.original.volume24, {
                style: "currency",
              })}
            </span>
          ),
        },
        {
          id: "actions",
          align: "center",
          Cell: ({ row }: any) => (
            <MenuPop
              items={[
                {
                  icon: "info",
                  label: t("ranking.table.buttonDetail"),
                  onClick: () => goToDetail(row.original.id),
                },
                {
                  icon: "storefront",
                  label: t("ranking.table.buttonMarkets"),
                  onClick: () => goToMarkets(row.original.id),
                },
              ]}
            />
          ),
        },
      ] as TableColumnsType,
    // eslint-disable-next-line
    [t]
  );

  const pagination = useMemo(
    () => ({
      count: countTotalCoins,
      rowsPerPage: paginationLimit,
      fetchData: (pageIndex: number) => {
        setSkeleton(true);
        setPaginationStart(pageIndex * paginationLimit);
      },
    }),
    [countTotalCoins]
  );

  const getTickersAllCoins = useCallback(async () => {
    const { data, totalCoins } = await CurrencyApiService.getTickersAllCoins({
      start: paginationStart,
    });

    setCoinList(data);
    setCountTotalCoins(totalCoins);
    setLoading(false);
    setSkeleton(false);
  }, [paginationStart]);

  const onUpdateList = () => {
    setLoading(true);
    getTickersAllCoins();
  };

  useEffect(() => {
    getTickersAllCoins();

    const interval = setInterval(getTickersAllCoins, RELOAD_DATA_INTERVAL);

    return () => {
      clearInterval(interval);
    };
  }, [getTickersAllCoins]);

  return (
    <div>
      <div className="flex items-center justify-between mb-5">
        <h2 className="text-2xl font-bold">{t("ranking.title")}</h2>

        <ButtonUpdate loading={loading} onClick={onUpdateList} />
      </div>

      <Table
        columns={columns}
        data={coinList}
        pagination={pagination}
        loading={skeleton}
      />
    </div>
  );
}

export default CurrencyRanking;
