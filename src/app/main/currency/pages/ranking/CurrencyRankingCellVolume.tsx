import clsx from "clsx";
import React from "react";
import Utils from "@core/utils/Utils";
// Relative Components
import { CurrencyRankingCellVolumeProps } from "../../Currency";

/**
 * @function CurrencyRankingCellVolume
 * @brief Componente que muestra el porcentaje del volumen, resaltando cuando es positivo o negativo
 * @param {number} volume Valor del volumen
 * @param {boolean} background Flag para habilitar el color de fondo
 */
function CurrencyRankingCellVolume(props: CurrencyRankingCellVolumeProps) {
  const { volume, background } = props;

  let color = "";
  let backgroundColor = "";

  if (volume > 0) {
    color = "text-green-400";
    backgroundColor = "bg-green-300";
  } else if (volume < 0) {
    color = "text-red-400";
    backgroundColor = "bg-red-300";
  }

  return (
    <span
      className={clsx(
        "font-bold p-1",
        color,
        background && clsx(backgroundColor, "bg-opacity-10 rounded-sm")
      )}
    >
      {Utils.formatNumber(volume, {
        style: "percent",
        maximumFractionDigits: 2,
      })}
    </span>
  );
}

export default CurrencyRankingCellVolume;
