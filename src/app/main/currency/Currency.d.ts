import CurrencyModel from "./model/CurrencyModel";

export interface CurrencyConverterProps {
  className?: string;
  currency: CurrencyModel;
}

export interface CurrencyGetTickerResponseType {
  status: boolean;
  currency: CurrencyModel;
}

type CurrencyDetailMainType = {
  currency: CurrencyModel;
  className?: string;
};

export interface CurrencyDetailInfoProps extends CurrencyDetailMainType {}

export interface CurrencyDetailDataProps extends CurrencyDetailMainType {}

export interface CurrencyDetailDataItemProps {
  title: string;
  icon: string;
  value: string | number;
}

export interface CurrencyDetailRankProps {
  className?: string;
  rank: number;
}

export interface CurrencyMarketCellPriceProps {
  firstValue: number;
  secondValue: number;
  secondValueLabel: string;
}

export interface CurrencyResponseType {
  id: string;
  rank: number;
  symbol: string;
  name: string;
  price_usd: string;
  percent_change_1h: string;
  percent_change_24h: string;
  percent_change_7d: string;
  market_cap_usd: string;
  volume24: number | string;
  volume24_native: string;
  csupply: string;
  tsupply: string;
  msupply: string;
}

export interface CurrencyRankingCellVolumeProps {
  volume: number;
  background?: boolean;
}

export interface CurrencyRankingResponseType {
  data: Array<CurrencyResponseType>;
  info: {
    coins_num: number;
    time: number;
  };
}
