export const CURRENCY_URL_MARKETS = "/coin/markets";
export const CURRENCY_URL_TICKER = "/ticker";
export const CURRENCY_URL_TICKERS_ALL_COINS = "/tickers";
