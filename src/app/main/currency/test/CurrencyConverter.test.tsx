import { act } from "react-dom/test-utils";
import { createTheme, ThemeProvider } from "@mui/material";
import { render } from "@testing-library/react";
import { themeOptions } from "@core/theme/theme";
import CurrencyApiService from "../api-service/CurrencyApiService";
import CurrencyConverter from "../pages/detail/CurrencyConverter";
import CurrencyModel from "../model/CurrencyModel";
import i18n from "@core/i18n/i18n";
import userEvent from "@testing-library/user-event";

jest.mock("../api-service/CurrencyApiService");

const mockCurrencyApiService: any = CurrencyApiService;

describe("CurrencyConverter", () => {
  let component: any;

  beforeAll(() => {
    i18n.init();
  });

  beforeEach(() => {
    const currency = new CurrencyModel({
      id: 90,
      symbol: "BTC",
      name: "Bitcoin",
      rank: 1,
      price_usd: 49082.54,
      percent_change_24h: -0.2,
      percent_change_1h: 0.24,
      percent_change_7d: -13.38,
      market_cap_usd: 927083032299.67,
      volume24: 23168710457.27,
      volume24_native: 472035.72,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: 200000,
    });

    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: true,
      currency,
    });

    act(() => {
      component = render(
        <ThemeProvider theme={createTheme(themeOptions.defaultDark)}>
          <CurrencyConverter currency={currency} />
        </ThemeProvider>
      );
    });
  });

  test("Convert from crypto currency to money", async () => {
    const input = component.container.querySelector("#cryptoCurrency");

    userEvent.clear(input);
    userEvent.type(input, "");
    userEvent.clear(input);
    userEvent.type(input, "a");
    userEvent.clear(input);
    userEvent.type(input, "2");
    userEvent.clear(input);
    userEvent.type(input, "2.123456");

    expect(component.container.querySelector("#cryptoCurrency")).toHaveValue(
      "2.12346"
    );

    expect(component.container.querySelector("#money")).toHaveValue(
      "104224.61406"
    );
  });

  test("Convert from money to crypto currency", async () => {
    const input = component.container.querySelector("#money");

    userEvent.clear(input);
    userEvent.type(input, "987654.888888");

    expect(component.container.querySelector("#money")).toHaveValue(
      "987654.88889"
    );

    expect(component.container.querySelector("#cryptoCurrency")).toHaveValue(
      "20.12233"
    );
  });
});
