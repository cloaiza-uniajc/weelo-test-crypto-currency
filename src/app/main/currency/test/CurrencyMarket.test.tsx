import { act } from "react-dom/test-utils";
import { renderWithRouter } from "@core/utils/TestUtils";
import { screen, fireEvent } from "@testing-library/react";
import CurrencyApiService from "../api-service/CurrencyApiService";
import CurrencyMarket from "../pages/market/CurrencyMarket";
import CurrencyMarketModel from "../model/CurrencyMarketModel";
import CurrencyModel from "../model/CurrencyModel";
import i18n from "@core/i18n/i18n";

jest.mock("../api-service/CurrencyApiService");

const mockCurrencyApiService: any = CurrencyApiService;

describe("CurrencyMarket", () => {
  beforeAll(() => {
    i18n.init();
  });

  test("Render and update", async () => {
    const currency = new CurrencyModel({
      id: 90,
      symbol: "BTC",
      name: "Bitcoin",
      rank: 1,
      price_usd: 49082.54,
      percent_change_24h: -0.2,
      percent_change_1h: 0.24,
      percent_change_7d: -13.38,
      market_cap_usd: 927083032299.67,
      volume24: 23168710457.27,
      volume24_native: 472035.72,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: 21000000,
    });

    const market1 = new CurrencyMarketModel({
      name: "Binance",
      base: "BTC",
      quote: "USDT",
      price: 45932.8,
      price_usd: 45932.8,
      volume: 64945.19302,
      volume_usd: 2983114561.9491,
      time: 1631159781,
    });

    const market2 = new CurrencyMarketModel({
      name: "Bitcoin.com",
      base: "BTC",
      quote: "USD",
      price: 45927.38,
      price_usd: 45927.38,
      volume: 0.1,
      volume_usd: 2427327489.6205,
      time: 1631159711,
    });

    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: true,
      currency,
    });

    mockCurrencyApiService.getMarketsForCoin.mockResolvedValue([
      market1,
      market2,
    ]);

    await act(async () => {
      renderWithRouter(<CurrencyMarket />, {
        path: "/:currencyId",
        route: "/90",
      });
    });

    const [row] = screen.getAllByText("Binance");

    expect(row).toBeInTheDocument();

    const buttonUpdate = screen.getByText("autorenew");

    await act(async () => {
      fireEvent.click(buttonUpdate);
    });
  });

  test("Render error", async () => {
    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: false,
      currency: null,
    });

    await act(async () => {
      renderWithRouter(<CurrencyMarket />, {
        path: "/:currencyId",
        route: "/90",
      });
    });

    const error = screen.getByText(i18n.t("common.errorMessage").toString());

    expect(error).toBeInTheDocument();
  });
});
