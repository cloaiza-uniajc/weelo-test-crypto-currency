import { act } from "react-dom/test-utils";
import { BrowserRouter } from "react-router-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import CurrencyApiService from "../api-service/CurrencyApiService";
import CurrencyModel from "../model/CurrencyModel";
import CurrencyRanking from "../pages/ranking/CurrencyRanking";
import i18n from "@core/i18n/i18n";

jest.mock("../api-service/CurrencyApiService");

const mockCurrencyApiService: any = CurrencyApiService;

describe("CurrencyRanking", () => {
  beforeAll(() => {
    i18n.init();
  });

  beforeEach(async () => {
    const dataCoin1 = new CurrencyModel({
      id: 90,
      symbol: "BTC",
      name: "Bitcoin",
      rank: 1,
      price_usd: 49059.81,
      percent_change_24h: -0.35,
      percent_change_1h: -0.06,
      percent_change_7d: -13.44,
      market_cap_usd: 926653771103.99,
      volume24: 23136237631.16884,
      volume24_native: 0,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: 21000000,
    });

    const dataCoin2 = new CurrencyModel({
      id: 90,
      symbol: "ETH",
      name: "Ethereum",
      rank: 1,
      price_usd: 49059.81,
      percent_change_24h: 0.35,
      percent_change_1h: 0,
      percent_change_7d: -13.44,
      market_cap_usd: 926653771103.99,
      volume24: 23136237631.16884,
      volume24_native: 0,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: 21000000,
    });

    mockCurrencyApiService.getTickersAllCoins.mockResolvedValue({
      data: [dataCoin1, dataCoin2],
      totalCoins: 100,
    });

    await act(async () => {
      render(
        <BrowserRouter>
          <CurrencyRanking />
        </BrowserRouter>
      );
    });
  });

  test("Render and update", async () => {
    const [row] = screen.getAllByText("Bitcoin");

    expect(row).toBeInTheDocument();

    const buttonUpdate = screen.getByText("autorenew");

    await act(async () => {
      fireEvent.click(buttonUpdate);
    });
  });

  test("Navigate to details", () => {
    const [buttonOptions] = screen.getAllByText("more_vert");

    fireEvent.click(buttonOptions);

    const [buttonDetail] = screen.getAllByText(
      i18n.t("ranking.table.buttonDetail").toString()
    );

    fireEvent.click(buttonDetail);
  });

  test("Navigate to markets", () => {
    const [buttonOptions] = screen.getAllByText("more_vert");

    fireEvent.click(buttonOptions);

    const [buttonMarkets] = screen.getAllByText(
      i18n.t("ranking.table.buttonMarkets").toString()
    );

    fireEvent.click(buttonMarkets);
  });
});
