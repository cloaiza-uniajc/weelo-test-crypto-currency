import Api from "@core/services/Api";
import { CurrencyResponseType, CurrencyRankingResponseType } from "../Currency";
import CurrencyApiService from "../api-service/CurrencyApiService";
import CurrencyMarketModel from "../model/CurrencyMarketModel";
import CurrencyModel from "../model/CurrencyModel";

jest.mock("@core/services/Api");

const mockApi: any = Api;

describe("CurrencyApiService", () => {
  describe("Get markets for coin", () => {
    test("Response ok", async () => {
      const market = new CurrencyMarketModel({
        name: "Binance",
        base: "BTC",
        quote: "USDT",
        price: 45932.8,
        price_usd: 45932.8,
        volume: 64945.19302,
        volume_usd: 2983114561.9491,
        time: 1631159781,
      });

      const marketList = [market, market];

      mockApi.requestAxios.mockResolvedValue(marketList);

      expect(await CurrencyApiService.getMarketsForCoin(1)).toEqual(marketList);
    });

    test("Response empty", async () => {
      mockApi.requestAxios.mockResolvedValue();

      expect(await CurrencyApiService.getMarketsForCoin(1)).toEqual([]);
    });
  });

  describe("Get tickers all coins", () => {
    test("Response ok", async () => {
      const responseCoin: CurrencyResponseType = {
        id: "90",
        symbol: "BTC",
        name: "Bitcoin",
        rank: 1,
        price_usd: "49059.81",
        percent_change_24h: "-0.35",
        percent_change_1h: "-0.06",
        percent_change_7d: "-13.44",
        market_cap_usd: "926653771103.99",
        volume24: 23136237631.16884,
        volume24_native: "0",
        csupply: "18888246.00",
        tsupply: "18888246",
        msupply: "21000000",
      };

      const response: CurrencyRankingResponseType = {
        data: [responseCoin, responseCoin],
        info: { coins_num: 6683, time: 1639141921 },
      };

      mockApi.requestAxios.mockResolvedValue(response);

      const dataCoin = new CurrencyModel({
        id: parseInt(responseCoin.id),
        rank: responseCoin.rank,
        symbol: responseCoin.symbol,
        name: responseCoin.name,
        price_usd: parseFloat(responseCoin.price_usd),
        percent_change_1h: parseFloat(responseCoin.percent_change_1h) / 100,
        percent_change_24h: parseFloat(responseCoin.percent_change_24h) / 100,
        percent_change_7d: parseFloat(responseCoin.percent_change_7d) / 100,
        market_cap_usd: parseFloat(responseCoin.market_cap_usd),
        volume24: parseFloat(responseCoin.volume24.toString()),
      });

      expect(await CurrencyApiService.getTickersAllCoins()).toEqual({
        data: [dataCoin, dataCoin],
        totalCoins: response.info.coins_num,
      });
    });

    test("Response empty", async () => {
      mockApi.requestAxios.mockResolvedValue();

      expect(await CurrencyApiService.getTickersAllCoins()).toEqual({
        data: [],
        totalCoins: 0,
      });
    });
  });

  describe("Get ticker currency detail", () => {
    test("Response ok", async () => {
      const responseCoin: CurrencyResponseType = {
        id: "90",
        symbol: "BTC",
        name: "Bitcoin",
        rank: 1,
        price_usd: "49082.54",
        percent_change_24h: "-0.20",
        percent_change_1h: "0.24",
        percent_change_7d: "-13.38",
        market_cap_usd: "927083032299.67",
        volume24: "23168710457.27",
        volume24_native: "472035.72",
        csupply: "18888246.00",
        tsupply: "18888246",
        msupply: "21000000",
      };

      mockApi.requestAxios.mockResolvedValue([responseCoin]);

      expect(await CurrencyApiService.getTickerCurrencyDetail(90)).toEqual({
        status: true,
        currency: new CurrencyModel({
          id: parseInt(responseCoin.id),
          rank: responseCoin.rank,
          symbol: responseCoin.symbol,
          name: responseCoin.name,
          price_usd: parseFloat(responseCoin.price_usd),
          percent_change_1h: parseFloat(responseCoin.percent_change_1h) / 100,
          percent_change_24h: parseFloat(responseCoin.percent_change_24h) / 100,
          percent_change_7d: parseFloat(responseCoin.percent_change_7d) / 100,
          market_cap_usd: parseFloat(responseCoin.market_cap_usd),
          volume24: parseFloat(responseCoin.volume24.toString()),
          volume24_native: parseFloat(responseCoin.volume24_native),
          csupply: parseFloat(responseCoin.csupply),
          tsupply: parseFloat(responseCoin.tsupply),
          msupply: parseFloat(responseCoin.msupply),
        }),
      });
    });

    test("Response empty", async () => {
      mockApi.requestAxios.mockResolvedValue();

      const responseCoin: any = {};

      expect(await CurrencyApiService.getTickerCurrencyDetail(90)).toEqual({
        status: false,
        currency: new CurrencyModel({
          id: parseInt(responseCoin.id),
          rank: responseCoin.rank,
          symbol: responseCoin.symbol,
          name: responseCoin.name,
          price_usd: parseFloat(responseCoin.price_usd),
          percent_change_1h: parseFloat(responseCoin.percent_change_1h) / 100,
          percent_change_24h: parseFloat(responseCoin.percent_change_24h) / 100,
          percent_change_7d: parseFloat(responseCoin.percent_change_7d) / 100,
          market_cap_usd: parseFloat(responseCoin.market_cap_usd),
          volume24: parseFloat(responseCoin.volume24),
          volume24_native: parseFloat(responseCoin.volume24_native),
          csupply: parseFloat(responseCoin.csupply),
          tsupply: parseFloat(responseCoin.tsupply),
          msupply: parseFloat(responseCoin.msupply),
        }),
      });
    });
  });
});
