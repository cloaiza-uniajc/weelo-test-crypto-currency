import { act } from "react-dom/test-utils";
import { createTheme, ThemeProvider } from "@mui/material";
import { renderWithRouter } from "@core/utils/TestUtils";
import { screen } from "@testing-library/react";
import { themeOptions } from "@core/theme/theme";
import CurrencyApiService from "../api-service/CurrencyApiService";
import CurrencyDetail from "../pages/detail/CurrencyDetail";
import CurrencyModel from "../model/CurrencyModel";
import i18n from "@core/i18n/i18n";

jest.mock("../api-service/CurrencyApiService");

const mockCurrencyApiService: any = CurrencyApiService;

describe("CurrencyDetail", () => {
  beforeAll(() => {
    i18n.init();
  });

  test("Render", async () => {
    const currency = new CurrencyModel({
      id: 90,
      symbol: "BTC",
      name: "Bitcoin",
      rank: 1,
      price_usd: 49082.54,
      percent_change_24h: -0.2,
      percent_change_1h: 0.24,
      percent_change_7d: -13.38,
      market_cap_usd: 927083032299.67,
      volume24: 23168710457.27,
      volume24_native: 472035.72,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: 200000,
    });

    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: true,
      currency,
    });

    await act(async () => {
      renderWithRouter(
        <ThemeProvider theme={createTheme(themeOptions.defaultDark)}>
          <CurrencyDetail />
        </ThemeProvider>,
        {
          path: "/:currencyId",
          route: "/90",
        }
      );
    });

    const text = screen.getByText("Bitcoin");

    expect(text).toBeInTheDocument();
  });

  test("Render and msupply undefined", async () => {
    const currency = new CurrencyModel({
      id: 90,
      symbol: "BTC",
      name: "Bitcoin",
      rank: 1,
      price_usd: 49082.54,
      percent_change_24h: -0.2,
      percent_change_1h: 0.24,
      percent_change_7d: -13.38,
      market_cap_usd: 927083032299.67,
      volume24: 23168710457.27,
      volume24_native: 472035.72,
      csupply: 18888246.0,
      tsupply: 18888246,
      msupply: NaN,
    });

    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: true,
      currency,
    });

    await act(async () => {
      renderWithRouter(
        <ThemeProvider theme={createTheme(themeOptions.defaultDark)}>
          <CurrencyDetail />
        </ThemeProvider>,
        {
          path: "/:currencyId",
          route: "/90",
        }
      );
    });

    const text = screen.getByText(i18n.t("currency.noApply").toString());

    expect(text).toBeInTheDocument();
  });

  test("Render error", async () => {
    mockCurrencyApiService.getTickerCurrencyDetail.mockResolvedValue({
      status: false,
      currency: null,
    });

    await act(async () => {
      renderWithRouter(
        <ThemeProvider theme={createTheme(themeOptions.defaultDark)}>
          <CurrencyDetail />
        </ThemeProvider>,
        {
          path: "/:currencyId",
          route: "/90",
        }
      );
    });

    const error = screen.getByText(i18n.t("common.errorMessage").toString());

    expect(error).toBeInTheDocument();
  });
});
