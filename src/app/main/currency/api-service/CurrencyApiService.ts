import Api from "@core/services/Api";
// Relative Components
import {
  CurrencyGetTickerResponseType,
  CurrencyRankingResponseType,
  CurrencyResponseType,
} from "../Currency";
import {
  CURRENCY_URL_MARKETS,
  CURRENCY_URL_TICKER,
  CURRENCY_URL_TICKERS_ALL_COINS,
} from "../Currency.consts";
import CurrencyMarketModel from "../model/CurrencyMarketModel";
import CurrencyModel from "../model/CurrencyModel";

/**
 * @class CurrencyApiService
 * @brief Clase de servicio para realizar peticiones al API para el módulo del Currency
 */
class CurrencyApiService {
  /**
   * Consulta los mercados de una moneda
   */
  async getMarketsForCoin(
    currencyId: number | string | undefined
  ): Promise<CurrencyMarketModel[]> {
    const response: CurrencyMarketModel[] = await Api.requestAxios({
      method: "GET",
      url: CURRENCY_URL_MARKETS,
      params: {
        id: currencyId,
      },
    });

    return (response || []).map((market) => {
      return new CurrencyMarketModel({
        name: market.name,
        base: market.base,
        quote: market.quote,
        price: market.price,
        price_usd: market.price_usd,
        volume: market.volume,
        volume_usd: market.volume_usd,
        time: market.time,
      });
    });
  }

  /**
   * Consulta el ranking de criptomonedas. Permite paginación.
   */
  async getTickersAllCoins({
    start = 0,
    limit = 100,
  }: {
    start?: number;
    limit?: number;
  } = {}): Promise<{ data: Array<CurrencyModel>; totalCoins: number }> {
    const response: CurrencyRankingResponseType = await Api.requestAxios({
      method: "GET",
      url: CURRENCY_URL_TICKERS_ALL_COINS,
      params: {
        limit,
        start,
      },
    });

    const data = (response?.data || []).map((coin) => {
      return new CurrencyModel({
        id: parseInt(coin.id),
        rank: coin.rank,
        symbol: coin.symbol,
        name: coin.name,
        price_usd: parseFloat(coin.price_usd),
        percent_change_1h: parseFloat(coin.percent_change_1h) / 100,
        percent_change_24h: parseFloat(coin.percent_change_24h) / 100,
        percent_change_7d: parseFloat(coin.percent_change_7d) / 100,
        market_cap_usd: parseFloat(coin.market_cap_usd),
        volume24: parseFloat(coin.volume24.toString()),
      });
    });

    const totalCoins = response?.info?.coins_num || 0;

    return { data, totalCoins };
  }

  /**
   * Consulta el detalle de una moneda
   */
  async getTickerCurrencyDetail(
    currencyId: number | string | undefined
  ): Promise<CurrencyGetTickerResponseType> {
    const response: CurrencyResponseType[] = await Api.requestAxios({
      method: "GET",
      url: CURRENCY_URL_TICKER,
      params: {
        id: currencyId,
      },
    });

    const status = !!response && response?.length > 0;
    const currency: any = status ? response[0] : {};

    return {
      status,
      currency: new CurrencyModel({
        id: parseInt(currency.id),
        rank: currency.rank,
        symbol: currency.symbol,
        name: currency.name,
        price_usd: parseFloat(currency.price_usd),
        percent_change_1h: parseFloat(currency.percent_change_1h) / 100,
        percent_change_24h: parseFloat(currency.percent_change_24h) / 100,
        percent_change_7d: parseFloat(currency.percent_change_7d) / 100,
        market_cap_usd: parseFloat(currency.market_cap_usd),
        volume24: parseFloat(currency.volume24),
        volume24_native: parseFloat(currency.volume24_native),
        csupply: parseFloat(currency.csupply),
        tsupply: parseFloat(currency.tsupply),
        msupply: parseFloat(currency.msupply),
      }),
    };
  }
}

export default new CurrencyApiService();
