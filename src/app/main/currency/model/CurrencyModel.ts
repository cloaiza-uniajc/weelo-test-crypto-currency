class CurrencyModel {
  id: number = 0;
  rank: number = 0;
  symbol: string = "";
  name: string = "";
  price_usd: number = 0;
  percent_change_1h: number = 0;
  percent_change_24h: number = 0;
  percent_change_7d: number = 0;
  market_cap_usd: number = 0;
  volume24: number = 0;
  volume24_native: number = 0;
  csupply: number = 0;
  tsupply: number = 0;
  msupply: number = 0;

  constructor({
    id = 0,
    rank = 0,
    symbol = "",
    name = "",
    price_usd = 0,
    percent_change_1h = 0,
    percent_change_24h = 0,
    percent_change_7d = 0,
    market_cap_usd = 0,
    volume24 = 0,
    volume24_native = 0,
    csupply = 0,
    tsupply = 0,
    msupply = 0,
  }) {
    this.id = id;
    this.rank = rank;
    this.symbol = symbol;
    this.name = name;
    this.price_usd = price_usd;
    this.percent_change_1h = percent_change_1h;
    this.percent_change_24h = percent_change_24h;
    this.percent_change_7d = percent_change_7d;
    this.market_cap_usd = market_cap_usd;
    this.volume24 = volume24;
    this.volume24_native = volume24_native;
    this.csupply = csupply;
    this.tsupply = tsupply;
    this.msupply = msupply;
  }
}

export default CurrencyModel;
