class CurrencyMarketModel {
  name: string = "";
  base: string = "";
  quote: string = "";
  price: number = 0;
  price_usd: number = 0;
  volume: number = 0;
  volume_usd: number = 0;
  time: number = 0;

  constructor({
    name = "",
    base = "",
    quote = "",
    price = 0,
    price_usd = 0,
    volume = 0,
    volume_usd = 0,
    time = 0,
  }) {
    this.name = name;
    this.base = base;
    this.quote = quote;
    this.price = price;
    this.price_usd = price_usd;
    this.volume = volume;
    this.volume_usd = volume_usd;
    this.time = time;
  }
}

export default CurrencyMarketModel;
