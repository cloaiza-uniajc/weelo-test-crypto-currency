import { Typography } from "@mui/material";
import React from "react";
import { useTranslation } from "react-i18next";

function Page404() {
  const { t } = useTranslation();

  return (
    <div className="flex flex-col items-center justify-center">
      <Typography color="error">
        <span className="block text-2xl font-bold p-5">{t("404.title")}</span>
      </Typography>

      <img src="/assets/error-404.png" alt="404" width="200" />
    </div>
  );
}

export default Page404;
