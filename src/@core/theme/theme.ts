import { ThemeOptions } from "@mui/material";

type ThemesOptionsType = {
  defaultLight: any;
  defaultDark: any;
};

export type ThemeType = {
  palette: ThemeOptions["palette"] & {
    type: "light" | "dark";
    background: { content?: string };
  };
};

type ThemesType = {
  [key in keyof ThemesOptionsType]: ThemeType;
};

export const themeOptions: ThemesType = {
  defaultLight: {
    palette: {
      type: "light",
      background: {
        content: "#7e7e7e",
        default: "#FFFFFF",
        paper: "#eaebf3",
      },
      primary: {
        light: "#50505a",
        main: "#282831",
        dark: "#000009",
        contrastText: "#FFFFFF",
      },
      secondary: {
        light: "#80d1fb",
        main: "#61C6FB",
        dark: "#438aaf",
        contrastText: "#222222",
      },
      text: {
        primary: "#222222",
        secondary: "#666666",
      },
      error: {
        light: "#f8877f",
        main: "#F44336",
        dark: "#d2190b",
        contrastText: "#FFFFFF",
      },
      info: {
        light: "#64aded",
        main: "#1e88e5",
        dark: "#1360a4",
        contrastText: "#FFFFFF",
      },
      success: {
        light: "#81c884",
        main: "#4caf50",
        dark: "#357a38",
        contrastText: "#FFFFFF",
      },
      warning: {
        light: "#ffc062",
        main: "#FF9800",
        dark: "#b36a00",
        contrastText: "#000000",
      },
      divider: "#000000",
    },
  },
  defaultDark: {
    palette: {
      type: "dark",
      background: {
        content: "#16172c",
        default: "#363747",
        paper: "#4d4e5c",
      },
      primary: {
        light: "#50505a",
        main: "#282831",
        dark: "#000009",
        contrastText: "#FFFFFF",
      },
      secondary: {
        light: "#80d1fb",
        main: "#61C6FB",
        dark: "#438aaf",
        contrastText: "#222222",
      },
      text: {
        primary: "#FFFFFF",
        secondary: "#DDDDDD",
      },
      error: {
        light: "#f8877f",
        main: "#F44336",
        dark: "#d2190b",
        contrastText: "#FFFFFF",
      },
      info: {
        light: "#64aded",
        main: "#1e88e5",
        dark: "#1360a4",
        contrastText: "#FFFFFF",
      },
      success: {
        light: "#81c884",
        main: "#4caf50",
        dark: "#357a38",
        contrastText: "#FFFFFF",
      },
      warning: {
        light: "#ffc062",
        main: "#FF9800",
        dark: "#b36a00",
        contrastText: "#000000",
      },
      divider: "#e0e0e0",
    },
  },
};
