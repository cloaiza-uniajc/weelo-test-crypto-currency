import Api from "./Api";
import axios from "axios";

jest.mock("axios");

const mockAxios: any = axios;

describe("Api", () => {
  test("Success request", () => {
    mockAxios.mockImplementation(() => Promise.resolve({ data: {} }));

    Api.requestAxios({ method: "GET", url: "/" });
  });

  test("Bad request", () => {
    mockAxios.mockImplementation(() => Promise.reject({ error: {} }));

    Api.requestAxios({ method: "GET", url: "/" });
  });
});
