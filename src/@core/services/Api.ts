import axios, { Method } from "axios";
// Relative components
import { URL_API } from "../constants/Constants";

/**
 * @class Api
 * @brief Clase con funciones para realizar llamados al api
 */
class Api {
  requestAxios({
    method,
    url,
    data,
    params,
  }: {
    method: Method;
    url: string;
    data?: object;
    params?: object;
  }) {
    return axios({
      method,
      url: URL_API + url,
      data,
      params,
    })
      .then((response: any) => response?.data)
      .catch((e: any) => e);
  }
}

export default new Api();
