import Utils from "./Utils";

describe("Utils", () => {
  test("Format date", () => {
    const date = new Date("2021-01-31 00:00:00");

    expect(Utils.formatDate(date)).toBe("2021/01/31 - 00:00:00");

    expect(Utils.formatDate(date, "YYYYMMDD")).toBe("20210131");
  });

  test("Format number", () => {
    expect(Utils.formatNumber(1000000)).toBe("1,000,000");

    expect(
      Utils.formatNumber(1000000.888, {
        style: "currency",
        currency: "USD",
        maximumFractionDigits: 2,
      })
    ).toBe("$1,000,000.89");
  });

  test("Get path by params", () => {
    expect(Utils.getPathByParams("/test/:id", { id: 1 })).toBe("/test/1");

    expect(Utils.getPathByParams("/test/:id?", {})).toBe("/test");
  });

  test("Round number", () => {
    expect(Utils.roundNumber(1.888889)).toBe(1.89);

    expect(Utils.roundNumber(1.888889, 5)).toBe(1.88889);
  });
});
