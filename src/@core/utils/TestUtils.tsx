import { render } from "@testing-library/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

/**
 * Permite renderizar un componente para pruebas el cual requiera ser renderizado bajo el router y con parámetros
 * @param element Componente a probar
 * @param params
 * @param params.path Ruta
 * @param params.route Url
 */
export const renderWithRouter = (
  element: any,
  { path, route }: { path: string; route: string }
) => {
  window.history.pushState({}, "", route);

  return render(
    <BrowserRouter>
      <Routes>
        <Route path={path} element={element} />
      </Routes>
    </BrowserRouter>
  );
};
