import moment from "moment";

const pathToRegexp = require("path-to-regexp");

/**
 * @class Utils
 * @brief Clase con funciones helpers
 */
class Utils {
  /**
   * Función que permite dar formato a una fecha
   * @param {Date} data Fecha a procesar
   * @param {string} format Formato para aplicar a la fecha
   * @return {string} Fecha formateada
   */
  formatDate(date: Date, format: string = "YYYY/MM/DD - HH:mm:ss") {
    return moment(date).format(format);
  }

  /**
   * Función da formato a un número dependiendo del estilo definido
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat
   * @param {number} number Número a procesar
   * @param {object} options
   * @param {string} options.style Formato para aplicar al número
   * @param {string} options.currency En caso de ser tipo currency permite definir la moneda
   * @param {number} options.maximumFractionDigits Númeor de decimales
   * @returns {string} Número formateado
   */
  formatNumber(
    number: number,
    {
      style = "decimal",
      currency = "USD",
      maximumFractionDigits = 0,
    }: {
      style?: Intl.NumberFormatPartTypes | "percent";
      currency?: string;
      maximumFractionDigits?: number;
    } = {}
  ) {
    return new Intl.NumberFormat("en-US", {
      style,
      currency,
      minimumFractionDigits: 0,
      maximumFractionDigits,
    }).format(number);
  }

  /**
   * Procesa una url con parámetros para reemplazarlos
   * @param {string} url Url con parámetros - /page/:pageId
   * @param {object} params Objeto con los parámetros a reemplazar de la cadena - { pageId: 1 }
   * @returns {string} Ruta con los parámetros reemplazados
   */
  getPathByParams(url: string, params: object) {
    const toPath = pathToRegexp.compile(url);

    return toPath(params);
  }

  /**
   * Redondea un número de acuerdo con los decimales
   * @param number
   * @param decimals
   * @returns {number} Número redondeado
   */
  roundNumber(number: number, decimals: number = 2) {
    const factor = 10 ** decimals;

    return Math.round(number * factor) / factor;
  }
}

export default new Utils();
