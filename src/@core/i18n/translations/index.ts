import es from "./es.json";
import en from "./en.json";

const resources = {
  es,
  en,
};

export default resources;
