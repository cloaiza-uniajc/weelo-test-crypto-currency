import { initReactI18next } from "react-i18next";
import i18n from "i18next";

import resources from "./translations";

import translationsConfig from "./translation.config";

i18n.use(initReactI18next).init({
  resources,
  lng: translationsConfig.es.lang,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
