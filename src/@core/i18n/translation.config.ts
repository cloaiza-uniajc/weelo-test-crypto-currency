const translations = {
  es: {
    label: "Español",
    lang: "es",
  },
  en: {
    label: "English",
    lang: "en",
  },
};

export default translations;
