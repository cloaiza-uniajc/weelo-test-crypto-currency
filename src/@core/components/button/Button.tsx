import { Button as ButtonMaterial, Tooltip } from "@mui/material";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";
// Relative Components
import { ButtonProps } from "../Components";

const useStyles = makeStyles({
  button: {
    textTransform: "none !important" as "none",
    fontWeight: "bold !important",
    paddingTop: "12px !important",
    paddingBottom: "12px !important",
  },
});

/**
 * @function Button
 * @brief Botón genérica
 */
function Button(props: ButtonProps) {
  const classes = useStyles();

  const { className, title, ...rest } = props;

  return (
    <Tooltip title={title || ""} arrow>
      <ButtonMaterial {...rest} className={clsx(classes.button, className)} />
    </Tooltip>
  );
}

export default Button;
