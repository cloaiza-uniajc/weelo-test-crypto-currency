import { act } from "react-dom/test-utils";
import { render, fireEvent, screen } from "@testing-library/react";
import Button from "./Button";
import React from "react";

describe("Button", () => {
  const mockHandler = jest.fn();
  const text = "Click me!";

  beforeEach(() => {
    act(() => {
      render(<Button children={text} onClick={mockHandler} />);
    });
  });

  test("Click", () => {
    const button = screen.getByText(text);

    expect(button).toBeInTheDocument();

    fireEvent.click(button);

    expect(mockHandler).toHaveBeenCalledTimes(1);
  });
});
