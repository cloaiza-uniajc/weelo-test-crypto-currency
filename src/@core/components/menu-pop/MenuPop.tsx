import { Icon, IconButton, Tooltip } from "@mui/material";
import { useTranslation } from "react-i18next";
import React, { useState } from "react";
// Relative Components
import { MenuPopProps } from "../Components";
import MenuPopOptionsList from "./MenuPopOptions";

/**
 * @function MenuPop
 * @brief Menú desplegable con listado de opciones
 * @param {array} list Opciones a mostrar
 * @param {string} icon Ícono del botón
 * @param {string} title Título para el tooltip
 */
function MenuPop(props: MenuPopProps) {
  const { icon, title, items } = props;

  const { t } = useTranslation();

  const [anchorEl, setAnchorEl] = useState(null as unknown as Element);

  function handleMenuClick(event: any) {
    setAnchorEl(event.currentTarget);
  }

  return (
    <div>
      <Tooltip title={title || t("common.options").toString()} arrow>
        <IconButton color="inherit" onClick={handleMenuClick} size="small">
          <Icon>{icon || "more_vert"}</Icon>
        </IconButton>
      </Tooltip>

      <MenuPopOptionsList
        items={items}
        anchorEl={anchorEl}
        setAnchorEl={setAnchorEl}
      />
    </div>
  );
}

MenuPop.defaultProps = {
  items: [],
};

export default MenuPop;
