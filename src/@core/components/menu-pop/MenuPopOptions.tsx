import {
  Icon,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} from "@mui/material";
import React from "react";
// Relative Components
import { MenuPopOptionsItemType, MenuPopOptionsProps } from "../Components";

/**
 * @function MenuPopOptions
 * @brief Menú desplegable con listado de opciones
 */
function MenuPopOptions(props: MenuPopOptionsProps) {
  const { anchorEl, items, setAnchorEl } = props;

  function onClose() {
    setAnchorEl(null);
  }

  return (
    <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={onClose}>
      {items.map((item: MenuPopOptionsItemType, index: number) => (
        <MenuItem
          key={`${index}-${item.label}`}
          onClick={() => {
            onClose();
            item.onClick();
          }}
          dense
        >
          {item.icon && (
            <ListItemIcon sx={{ color: "inherit" }}>
              <Icon>{item.icon}</Icon>
            </ListItemIcon>
          )}

          <ListItemText primary={item.label} />
        </MenuItem>
      ))}
    </Menu>
  );
}

export default MenuPopOptions;
