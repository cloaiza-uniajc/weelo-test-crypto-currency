import { act } from "react-dom/test-utils";
import { render, fireEvent, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import MenuPop from "./MenuPop";
import React from "react";

describe("MenuPop", () => {
  let mockHandler: any;
  const itemToClick = "Item to click";

  beforeEach(() => {
    mockHandler = jest.fn();

    i18n.init();

    act(() => {
      render(
        <MenuPop
          items={[
            { icon: "", label: itemToClick, onClick: mockHandler },
            { icon: "star", label: "test", onClick: mockHandler },
          ]}
        />
      );
    });
  });

  test("Open menu and option click", () => {
    act(() => {
      const button = screen.getByRole("button");

      fireEvent.click(button);
    });

    act(() => {
      const item = screen.getByText(itemToClick);

      fireEvent.click(item);

      expect(mockHandler).toHaveBeenCalledTimes(1);
    });
  });
});
