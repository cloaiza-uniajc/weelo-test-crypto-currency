import { act } from "react-dom/test-utils";
import { render, fireEvent, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import React from "react";
import Table from "./Table";

describe("Table", () => {
  const mockHandler = jest.fn();

  const columnIdToSort = "id";
  const columnToSort = "Column id";

  const columns = [
    { Header: columnToSort, accesor: columnIdToSort, id: columnIdToSort },
    {
      Header: "Name",
      id: "name",
      Cell: ({ row }: any) => row.original.display_name,
    },
  ];

  const data = [
    {
      id: 1,
      display_name: "Cristian",
    },
    {
      id: 2,
      display_name: "Andrés",
    },
  ];

  const sortBy = [{ id: columnIdToSort, desc: true }];

  const pagination = {
    count: 10,
    rowsPerPage: 2,
    fetchData: mockHandler,
  };

  beforeEach(() => {
    i18n.init();
  });

  test("Table loading", () => {
    act(() => {
      render(<Table columns={columns} data={[]} loading />);
    });
  });

  test("Table and sort column", () => {
    let component: any = null;

    act(() => {
      component = render(
        <Table columns={columns} data={data} sortBy={sortBy} />
      );
    });

    act(() => {
      const column = component.getByText(columnToSort);

      fireEvent.click(column);
    });
  });

  test("Empty table", () => {
    act(() => {
      render(<Table columns={columns} data={[]} />);
    });
  });

  test("Table pagination", () => {
    act(() => {
      render(
        <Table columns={columns} data={data} pagination={pagination} loading />
      );
    });

    act(() => {
      render(<Table columns={columns} data={data} pagination={pagination} />);
    });
  });

  test("Table pagination click", () => {
    act(() => {
      render(<Table columns={columns} data={data} pagination={pagination} />);
    });

    act(() => {
      const nextPage = screen.getByText("keyboard_arrow_right");

      fireEvent.click(nextPage);
    });

    act(() => {
      const nextPage = screen.getByText("keyboard_arrow_left");

      fireEvent.click(nextPage);
    });

    act(() => {
      const nextPage = screen.getByText("last_page");

      fireEvent.click(nextPage);
    });

    act(() => {
      const nextPage = screen.getByText("first_page");

      fireEvent.click(nextPage);
    });
  });
});
