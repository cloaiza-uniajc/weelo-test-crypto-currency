import { Icon, IconButton, Tooltip } from "@mui/material";
import React from "react";
// Relative Components
import { TablePaginationActionButtonProps } from "../Components";

/**
 * @function TablePaginationActionButton
 * @brief Botón dinámico para las acciones de la paginación
 */
function TablePaginationActionButton(props: TablePaginationActionButtonProps) {
  const { title, disabled, onClick, icon } = props;

  return (
    <Tooltip title={title} arrow>
      <span>
        <IconButton color="inherit" onClick={onClick} disabled={disabled}>
          <Icon>{icon}</Icon>
        </IconButton>
      </span>
    </Tooltip>
  );
}

export default TablePaginationActionButton;
