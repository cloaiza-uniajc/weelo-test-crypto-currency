import {
  useGlobalFilter,
  usePagination,
  useRowSelect,
  useSortBy,
  useTable,
} from "react-table";
import { CircularProgress, TableFooter, TablePagination } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useTranslation } from "react-i18next";
import Paper from "@mui/material/Paper";
import React, { useEffect } from "react";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableMUI from "@mui/material/Table";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
// Relative Components
import { TableProps } from "../Components";
import TablePaginationActions from "./TablePaginationActions";

const useStyles = makeStyles(() => ({
  tableHeaderCell: {
    fontWeight: "bold !important",
  },
}));

/**
 * @function Table
 * @brief Tabla genérica con paginación
 * @param {array} columns Listado de columnas y configuración de las celdas
 * @param {array} data Listado de datos a mostrar
 * @param {boolean} loading Indica que la data está siendo consultada
 * @param {object} pagination Objeto con la configuración para la paginación
 * @param {number} pagination.count Número total de registros
 * @param {number} pagination.rowsPerPage Registros por página
 * @param {function} pagination.ferchData Función de callbaxk que pasa el número de la nueva página seleccionada
 */
function Table(props: TableProps) {
  const classes = useStyles();

  const { columns, data, sortBy, loading, pagination } = props;

  const { t } = useTranslation();

  const {
    getTableProps,
    getTableBodyProps,
    // Cabecera
    headerGroups,
    // Filas
    page,
    prepareRow,
    // Paginación
    pageCount,
    gotoPage,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    state: { pageIndex },
  } = useTable(
    {
      columns: columns as any,
      data,
      initialState: { pageIndex: 0, sortBy },
      manualPagination: true,
      autoResetPage: false,
      // Si se requiera paginación se calcula el total de páginas con base en el total de files y de registros por página
      pageCount: pagination
        ? Math.ceil(pagination.count / pagination.rowsPerPage)
        : 0,
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    useRowSelect
  );

  const TblBody = () => {
    if (loading) {
      return (
        <TableRow>
          <TableCell colSpan={columns.length} align="center" className="h-96">
            <CircularProgress />
          </TableCell>
        </TableRow>
      );
    }

    return (
      <>
        {page.length === 0 ? (
          <TableRow>
            <TableCell colSpan={columns.length} align="center" className="h-96">
              <span className="text-xl font-bold">{t("table.emptyList")}</span>
            </TableCell>
          </TableRow>
        ) : (
          page.map((row: any) => {
            prepareRow(row);

            return (
              <TableRow {...row.getRowProps()} className="truncate" hover>
                {row.cells.map((cell: any) => (
                  <TableCell
                    {...cell.getCellProps()}
                    align={cell.column.align}
                    className={cell.column.className}
                  >
                    {cell.render("Cell")}
                  </TableCell>
                ))}
              </TableRow>
            );
          })
        )}
      </>
    );
  };

  /**
   * Cambio de página en la tabla
   */
  useEffect(() => {
    pagination?.fetchData(pageIndex);
  }, [pagination, pageIndex]);

  return (
    <TableContainer component={Paper}>
      <TableMUI {...getTableProps()}>
        <TableHead>
          {headerGroups.map((headerGroup: any) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: any) => (
                <TableCell
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  align={column.align}
                  classes={{ root: classes.tableHeaderCell }}
                >
                  <TableSortLabel
                    active={column.isSorted}
                    direction={column.isSortedDesc ? "desc" : "asc"}
                  >
                    {column.render("Header")}
                  </TableSortLabel>
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>

        <TableBody {...getTableBodyProps()}>
          <TblBody />
        </TableBody>

        {!!pagination && (
          <TableFooter>
            <TableRow>
              <TablePagination
                page={pageIndex}
                count={pagination?.count}
                rowsPerPage={pagination?.rowsPerPage}
                onPageChange={() => undefined}
                ActionsComponent={() => (
                  <TablePaginationActions
                    loading={loading}
                    pageCount={pageCount}
                    gotoPage={gotoPage}
                    previousPage={previousPage}
                    nextPage={nextPage}
                    canPreviousPage={canPreviousPage}
                    canNextPage={canNextPage}
                  />
                )}
                labelDisplayedRows={({ from, to, count }) =>
                  t("table.showingFromToCount", { from, to, count })
                }
                rowsPerPageOptions={[]}
                labelRowsPerPage=""
              />
            </TableRow>
          </TableFooter>
        )}
      </TableMUI>
    </TableContainer>
  );
}

Table.defaultProps = {
  sortBy: [],
};

export default Table;
