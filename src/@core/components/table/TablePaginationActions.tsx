import { useTranslation } from "react-i18next";
import React from "react";
// Relative Components
import { TablePaginationActionsProps } from "../Components";
import TablePaginationActionButton from "./TablePaginationActionButton";

/**
 * @function TablePaginationActions
 * @brief Componentes de botones para la navegación en la paginación
 * @param {number} pageCount Total de páginas
 * @param {function} gotoPage Función para ir a una página determinada
 * @param {function} previousPage Función para ir a la página anterior
 * @param {function} nextPage Función para ir a la página siguiente
 * @param {boolean} canPreviousPage Flag que valida si se puede retroceder en la paginación
 * @param {boolean} canNextPage Flag que valida si se puede avanzar en la paginación
 * @param {boolean} loading Flag que indica que se está consultando el listado
 */
function TablePaginationActions(props: TablePaginationActionsProps) {
  const {
    pageCount,
    gotoPage,
    previousPage,
    nextPage,
    canPreviousPage,
    canNextPage,
    loading,
  } = props;

  const { t } = useTranslation();

  return (
    <div className="flex">
      <TablePaginationActionButton
        title={t("table.pagination.firstPage")}
        icon="first_page"
        onClick={() => gotoPage(0)}
        disabled={loading || !canPreviousPage}
      />

      <TablePaginationActionButton
        title={t("table.pagination.previousPage")}
        icon="keyboard_arrow_left"
        onClick={() => previousPage()}
        disabled={loading || !canPreviousPage}
      />

      <TablePaginationActionButton
        title={t("table.pagination.nextPage")}
        icon="keyboard_arrow_right"
        onClick={() => nextPage()}
        disabled={loading || !canNextPage}
      />

      <TablePaginationActionButton
        title={t("table.pagination.lastPage")}
        icon="last_page"
        onClick={() => gotoPage(pageCount - 1)}
        disabled={loading || !canNextPage}
      />
    </div>
  );
}

export default TablePaginationActions;
