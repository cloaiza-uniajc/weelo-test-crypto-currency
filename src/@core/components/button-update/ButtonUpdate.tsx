import { Icon, IconButton, Tooltip } from "@mui/material";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import React from "react";
// Relative Components
import { ButtonUpdateProps } from "../Components";

/**
 * @function ButtonUpdate
 * @brief Botón que hace referencia a realizar una actualización. Al hacer clic muestra animación que indica que está cargando
 */
function ButtonUpdate(props: ButtonUpdateProps) {
  const { loading, onClick } = props;

  const { t } = useTranslation();

  return (
    <Tooltip title={t("common.update").toString()} arrow>
      <IconButton color="secondary" onClick={loading ? undefined : onClick}>
        <Icon className={clsx(loading && "animate-spin")}>autorenew</Icon>
      </IconButton>
    </Tooltip>
  );
}

export default ButtonUpdate;
