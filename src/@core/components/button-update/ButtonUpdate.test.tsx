import { act } from "react-dom/test-utils";
import { render, fireEvent, screen } from "@testing-library/react";
import ButtonUpdate from "./ButtonUpdate";
import i18n from "@core/i18n/i18n";
import React from "react";

describe("ButtonUpdate", () => {
  let mockHandler: any;

  beforeEach(() => {
    mockHandler = jest.fn();
    i18n.init();
  });

  test("Click enabled", () => {
    act(() => {
      render(<ButtonUpdate onClick={mockHandler} loading={false} />);
    });

    const button = screen.getByRole("button");

    fireEvent.click(button);

    expect(mockHandler).toHaveBeenCalledTimes(1);
  });

  test("Click disabled", () => {
    act(() => {
      render(<ButtonUpdate onClick={mockHandler} loading />);
    });

    const button = screen.getByRole("button");

    fireEvent.click(button);

    expect(mockHandler).toHaveBeenCalledTimes(0);
  });
});
