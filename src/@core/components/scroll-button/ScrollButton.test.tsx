import { act } from "react-dom/test-utils";
import { render, fireEvent, screen } from "@testing-library/react";
import i18n from "@core/i18n/i18n";
import React from "react";
import ScrollButton from "./ScrollButton";

describe("ScrollButton", () => {
  const idDivContainer = "div_test";

  beforeEach(() => {
    i18n.init();
  });

  test("Scroll and click", () => {
    let component: any = null;

    act(() => {
      component = render(
        <div id={idDivContainer}>
          <ScrollButton idDivContainer={idDivContainer} />
        </div>
      );
    });

    const scrollContainer = component.container.querySelector(
      "#" + idDivContainer
    );

    expect(scrollContainer.firstChild).toHaveClass("hidden");

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 10,
        scroll: () =>
          fireEvent.scroll(scrollContainer, {
            target: {
              scrollTop: 0,
            },
          }),
      },
    });

    expect(scrollContainer.firstChild).toHaveClass("block");

    const button = screen.getByText("arrow_upward");

    fireEvent.click(button);

    expect(scrollContainer.firstChild).toHaveClass("hidden");
  });
});
