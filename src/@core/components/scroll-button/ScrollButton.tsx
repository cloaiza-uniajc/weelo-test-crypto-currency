import { Icon } from "@mui/material";
import Button from "@core/components/button/Button";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { ScrollButtonProps } from "../Components";

/**
 * @function ScrollButton
 * @brief Componte que agrega un botón para ir al principio de la página cuando se ha hecho scroll
 * @param {string} idDivContainer Identificador del contenedor en donde se hará scroll
 */
function ScrollButton(props: ScrollButtonProps) {
  const { idDivContainer } = props;

  const [scroll, setScroll] = useState(false);

  /**
   * Realiza el toggle del scroll cuando detecta cambios en el scroll
   * @param event
   */
  function scrollAction(event: any) {
    const { scrollTop } = event.target;

    setScroll(scrollTop > 0);
  }

  /**
   * Realiza scroll al inicio de la página
   */
  function scrollToTop() {
    const container = document.getElementById(idDivContainer);

    container?.scroll(0, 0);
  }

  useEffect(() => {
    const container = document.getElementById(idDivContainer);

    container?.addEventListener("scroll", scrollAction);

    return () => {
      container?.removeEventListener("scroll", scrollAction);
    };
  }, [idDivContainer]);

  return (
    <div
      className={clsx(
        "absolute z-50 bottom-0 right-0 mx-8 mb-4 shadow-md",
        scroll ? "block" : "hidden"
      )}
    >
      <Button
        variant="contained"
        size="large"
        color="secondary"
        onClick={scrollToTop}
      >
        <Icon color="inherit">arrow_upward</Icon>
      </Button>
    </div>
  );
}

export default ScrollButton;
