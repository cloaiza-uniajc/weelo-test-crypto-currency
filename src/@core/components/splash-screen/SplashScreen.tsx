import { CircularProgress } from "@mui/material";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles({
  fadeOut: {
    opacity: 0,
    transition: "all 500ms linear 1s",
  },
});

function SplashScreen() {
  const classes = useStyles();

  const [fade, setFade] = useState(false);
  const [hidden, setHidden] = useState(false);

  useEffect(() => {
    let timer: any;

    if (fade) {
      timer = setTimeout(() => {
        setHidden(true);
      }, 1500);
    } else {
      timer = setTimeout(() => {
        setFade(true);
      }, 1000);
    }

    return () => {
      clearTimeout(timer);
    };
  }, [fade]);

  console.log(fade, hidden);

  if (hidden) {
    return null;
  }

  return (
    <div
      className={clsx(
        "bg-gray-700 flex flex-col items-center justify-center absolute w-full h-full z-50",
        fade && classes.fadeOut
      )}
    >
      <img
        src="/assets/favicon.png"
        width={100}
        alt="logo"
        className="animate-bounce m-3"
      />

      <CircularProgress color="secondary" size={30} />
    </div>
  );
}

export default SplashScreen;
