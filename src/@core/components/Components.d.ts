import { TableCellProps } from "@mui/material";
import { ButtonProps as ButtonMaterialProps } from "@mui/material";
import { Column, SortingRule } from "react-table";

export type ButtonProps = ButtonMaterialProps & {
  to?: string;
  component?: any;
};

export interface ButtonUpdateProps {
  loading: boolean;
  onClick(): void;
}

export type MenuPopOptionsItemType = {
  icon?: string;
  label: string;
  onClick(): void;
};

export interface MenuPopProps {
  icon?: string;
  title?: string;
  items: Array<MenuPopOptionsItemType>;
}

export interface MenuPopOptionsProps {
  anchorEl: Element;
  items: Array<MenuPopOptionsItemType>;
  setAnchorEl(element: any): void;
}

export interface ScrollButtonProps {
  idDivContainer: string;
}

export type TableColumnsType = Column<
  Column<{ className?: string; align?: TableCellProps["align"] } & object>
>[];

export interface TableProps {
  columns: TableColumnsType;
  data: Array<any>;
  sortBy?: SortingRule<object>[];
  loading?: boolean;
  pagination?: {
    count: number;
    rowsPerPage: number;
    fetchData(pageIndex: number): void;
  };
}

export interface TablePaginationActionsProps {
  pageCount: number;
  gotoPage(page: number): void;
  previousPage(): void;
  nextPage(): void;
  canPreviousPage: boolean;
  canNextPage: boolean;
  loading?: boolean;
}

export interface TablePaginationActionButtonProps {
  disabled: boolean;
  icon: string;
  onClick(): void;
  title: string;
}
