# Prueba de Desarrollo (Million and Up & Weelo)

## Desarrollado por Cristian Loaiza

---

### Descripción:

Aplicación para revisión de criptomonedas y conversión de criptomonedas a dólares:

- Demo: [http://18.205.157.200:5000](http://18.205.157.200:5000). *Al momento de realizar peticiones al API para evitar tener errores de CORS Policies, se debe de tener instalada alguna extensión en el navegador que permita realizar estas peticiones. Extensión sugerida:* 

  - **Allow CORS: Access-Control-Allow-Origin**.
  - [Extensión](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=es).
  - [Video explicativo](https://www.youtube.com/watch?v=KruSUqLdxQA&ab_channel=eccoremproject).
  - [Página](https://mybrowseraddon.com/access-control-allow-origin.html).


- API utilizada: [https://www.coinlore.com/cryptocurrency-data-api](https://www.coinlore.com/cryptocurrency-data-api).

- Diseño responsive.

- Funcionalidad para cambiar el tema entre claro/oscuro.

- Funcionalidad de selección del idioma de la página.

- Muestra el resumen global de las monedas, capitalización, mercados, entre otros datos.

- Cuenta con una página principal que muestra una tabla con paginación para listar todas las criptomonedas y su información.

- De cada criptomoneda permite visualizar el detalle.

- En la página de detalle de una moneda se tiene un conversor del valor de la moneda seleccionada a dólares.

- Permite visualizar el listado de mercados de una moneda.

- Cada cierto tiempo la aplicación actualiza la información mostrada.

- El proyecto cuenta con CI/CD mediante Pipelines de Bitbucket y CodeDeploy de AWS. Además, cuenta con análisis de código mediante SonarCloud.

---

### Tecnologías, Librerías y Herramientas:

- React JS
- Npm
- Typescript
- React Router
- Material UI
- Tailwind CSS
- Axios
- I18n
- React Table
- Moment JS
- Clsx
- React Testing

---

## Ejecución del proyecto de manera local

---

### Instalación de dependencias y librerías del proyecto:

```
npm install
```

### Ejecución del proyecto en modo de desarrollo:

```
npm start
```

### Ejecución de pruebas:

```
npm test
```
